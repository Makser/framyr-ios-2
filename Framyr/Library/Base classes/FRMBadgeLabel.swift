//
//  FRMBadgeLabel.swift
//  Framyr
//
//  Created by Admin on 28.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


private let kBadgeTag = 46587


class FRMBadgeLabel: UILabel {


  override var bounds: CGRect {
    didSet {
      layer.cornerRadius = min(bounds.size.width, bounds.size.height) / 2
    }
  }
  
  
  init(parentView: UIView, relativePosition: CGPoint) {
    super.init(frame: CGRectMake(0, 0, 1, 1))

    parentView.addSubview(self)
    
    translatesAutoresizingMaskIntoConstraints = false
    let xConstraint = NSLayoutConstraint(item: self, attribute: .CenterX, relatedBy: .Equal, toItem: parentView, attribute: .CenterX, multiplier: 1, constant: parentView.frame.size.width * relativePosition.x)
    let yConstraint = NSLayoutConstraint(item: self, attribute: .CenterY, relatedBy: .Equal, toItem: parentView, attribute: .CenterY, multiplier: 1, constant: parentView.frame.size.height * relativePosition.y)
    
    parentView.addConstraints([xConstraint, yConstraint])
    
    tag = kBadgeTag
    configureLabel()
  }

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    
    configureLabel()
  }
  

  private func configureLabel() {
    
    layer.masksToBounds = true
    
    backgroundColor = AppColors.accent0.color
    textColor = AppColors.white.color
    textAlignment = .Center
    font = AppFonts.HelveticaNeueCyrRoman.font(12)
  }
  
  
  override func drawTextInRect(rect: CGRect) {
    let insets = UIEdgeInsetsMake(2, 0, 0, 0)
    super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
  }
  
  
  override func intrinsicContentSize() -> CGSize {
    var size = super.intrinsicContentSize()
    size.height += 8
    size.width += 8
    
    return size
  }
  
  
}


extension UIView {

  
  func addBadge(relativeCenterPosition: CGPoint = CGPointMake(-0.35, 0.3)) {
    if let _ = viewWithTag(kBadgeTag) { return }
    
    let _ = FRMBadgeLabel(parentView: self, relativePosition: relativeCenterPosition)
  }
  
  
  func setBadgeNumber(number: Int) {
    if let badgeLabel = viewWithTag(kBadgeTag) as? FRMBadgeLabel {
      if number == 0 { badgeLabel.removeFromSuperview() }
      else { badgeLabel.text = "\(number)" } }
    else {
      if number > 0 {
        addBadge()
        setBadgeNumber(number) } }
    
  }
  
  
}