//
//  FRMBaseButton.swift
//  Framyr
//
//  Created by Admin on 22.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMBaseButton: UIButton {

  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    backgroundColor = AppColors.accent.color
    setTitleColor(AppColors.white.color, forState: .Normal)
    titleLabel?.font = AppFonts.HelveticaNeueCyrLight.font(17)
  }
  
  
  override var enabled: Bool {
    didSet { alpha = enabled ? 1.0 : 0.5 }
  }
  

}
