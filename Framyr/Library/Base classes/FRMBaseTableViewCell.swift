//
//  FRMBaseTableViewCell.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMBaseTableViewCell: UITableViewCell {


  @IBOutlet private weak var view: UIView!
  @IBOutlet internal weak var titleLabel: UILabel?
  
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    selectionStyle = .None
    
    view.layer.borderWidth = 1
    view.layer.cornerRadius = 2
    view.layer.masksToBounds = false
    view.layer.borderColor = UIColor.lightGrayColor().CGColor
    view.layer.shadowRadius = 2
    view.layer.shadowOffset = CGSizeMake(2, 2)
    view.layer.shadowOpacity = 0.3

    view.addGrid()
  }
  
  
  override func setSelected(selected: Bool, animated: Bool) {
    if selected {
      view.layer.shadowOpacity = 0
      view.backgroundColor = AppColors.accent.color
      titleLabel?.textColor = AppColors.white.color
    }
    else {
      view.layer.shadowOpacity = 0.3
      view.backgroundColor = AppColors.white.color
      titleLabel?.textColor = AppColors.color1.color
    }
    
  }
  

}
