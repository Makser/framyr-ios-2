//
//  FRMSupportView.swift
//  Framyr
//
//  Created by Admin on 28.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMSupportView: UIView {

  override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
    if(!self.clipsToBounds && !self.hidden && self.alpha > 0.0){
      let subviews = self.subviews.reverse()
      for member in subviews {
        var subPoint = member.convertPoint(point, fromView: self)
        if let result:UIView = member.hitTest(subPoint, withEvent:event) {
          return result;
        }
      }
    }
    return nil
  }

}
