//
//  FRMBaseViewController.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMBaseViewController: UIViewController {

  
  //  MARK: -
  //  MARK: properties
  var titleLabel: UILabel!

  
}



//  MARK: -
//  MARK: VLC
extension FRMBaseViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
  
    view.addGestureRecognizer(revealViewController().panGestureRecognizer())
    
    navigationController?.navigationBar.barTintColor = AppColors.accent.color
    navigationController?.navigationBar.tintColor = AppColors.white.color
    navigationController?.navigationBar.opaque = true
    navigationController?.navigationBar.translucent = false
    
    titleLabel = UILabel(frame: CGRectMake(0, 0, 250, 40))
    titleLabel.textAlignment = .Left
    titleLabel.textColor = AppColors.white.color
    titleLabel.font = AppFonts.HelveticaNeueCyrLight.font(14)
    titleLabel.adjustsFontSizeToFitWidth = true
    navigationItem.titleView = titleLabel
    
    if navigationController?.viewControllers.count == 1 { addMenuButton() }
    if navigationController?.viewControllers.count > 1 { addBackButton() }
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    configureRightBarButton()
  }
  
  
  func configureRightBarButton() {
    addBasketButton()
  }
  
  
}



extension FRMBaseViewController {
  
  
  func makeButton(icon: Icomoon, target: AnyObject?, action: Selector) -> UIBarButtonItem {
    let button = UIButton(frame: CGRectMake(0, 0, 30, 30))
    button.titleLabel?.font = UIFont(name: "icomoon", size: 30)
    
    button.setTitle(icon.rawValue, forState: .Normal)
    button.addTarget(target, action: action, forControlEvents: UIControlEvents.TouchUpInside)
    return UIBarButtonItem(customView: button)
  }
  
  
  func makeSpacer(width: CGFloat) -> UIBarButtonItem {
    let spacer = UIBarButtonItem(barButtonSystemItem: .FixedSpace, target: nil, action: nil)
    spacer.width = width
    return spacer
  }
  
  
  func addMenuButton() {
    let menuButton = makeButton(Icomoon.Menu, target: revealViewController(), action: Selector("revealToggle:"))
    let spacer = makeSpacer(-10)
    
    navigationItem.leftBarButtonItems = [spacer, menuButton]
    
  }
  
  
  func addBackButton() {
    let backButton = makeButton(Icomoon.Back, target: navigationController, action: Selector("popViewControllerAnimated:"))
    let spacer = makeSpacer(-10)
    
    navigationItem.leftBarButtonItems = [spacer, backButton]
  
  }
  
  
  func addBasketButton() {
    let basketButton = makeButton(Icomoon.Shop, target: self, action: Selector("pushBasket"))
    let spacer = makeSpacer(-10)
    
    if let basketView = basketButton.customView {
      basketView.setBadgeNumber(Singletone.sharedInstance.basket.totalCount)
    }

    navigationItem.rightBarButtonItems = [spacer, basketButton]
    
  }
  
  
  func addGearButton(selector: Selector) {
    let gearButton = makeButton(Icomoon.City, target: self, action: selector)
    let spacer = makeSpacer(-10)
    
    navigationItem.rightBarButtonItems = [spacer, gearButton]
    
  }
  
  
  func pushBasket() {
    let basketVC = Storyboards.Main.instantiateBasket()
    navigationController?.pushViewController(basketVC, animated: true)
  }
  
  
}