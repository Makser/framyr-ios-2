//
//  FRMAPILayer.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMAPILayer: NetworkLayer {

  
  enum CategoryType: String {
    case FinezaPuerta = "fineza"
    case Framyr = "framyr"
    case News = "news"
    case Colors = "colors"
    case DoorSystems = "systems"
    case StainedGlasses = "vitrazh"
    case Shops = "shops"
    case Interior = "interior"
    
    static var all: [CategoryType] { return [.FinezaPuerta, .Framyr, .News, .Colors, .DoorSystems, .StainedGlasses, .Shops, .Interior] }
  }
  
  
  
  //  MARK: -
  //  MARK: properties
  static let sharedInstance = FRMAPILayer()
  
  
  //  MARK: -
  //  MARK: methods
  func checkLastUpdate(completion: (NSDate?)->()) {
//    completion(nil); return; NSLog("NO WAY!")
    
    guard let url = AppURL.UpdateDate.url else { return }
    self.responseSerializer = AFHTTPResponseSerializer()
    
    request(nil, endpoint: url.absoluteString, method: .GET) { (response, error) -> () in
      guard let dateData = response as? NSData else { completion(nil); return }
      guard let stringDate = String(data: dateData, encoding: NSUTF8StringEncoding) else { completion(nil); return }
      
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "dd.MM.yyyy hh:mm:ss"
      dateFormatter.timeZone = NSTimeZone(abbreviation: "GMT")
      
      guard let lastUpdateDate = dateFormatter.dateFromString(stringDate) else { completion(nil); return }
      
      completion(lastUpdateDate)
    }
    
  }
  
  
  func getUpdateList(date: NSDate, completion: ([CategoryType: Int])->()) {
    guard let url = AppURL.UpdateList.url else { return }
    self.requestSerializer = AFJSONRequestSerializer()
//    self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>
    
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "dd.MM.yyyy%hh:mm:ss"
    let dateString = dateFormatter.stringFromDate(date)
    
    request(["date" : dateString], endpoint: url.absoluteString, method: .GET) { (response, error) -> () in
      guard let updatesData = response as? NSData else { completion([:]); return }
      do {
        let updatesDict = try NSJSONSerialization.JSONObjectWithData(updatesData, options: .AllowFragments) as? [String: Int]
        
        var translatedUpdatesDict = [CategoryType: Int] ()
        for type in CategoryType.all {
          let value = updatesDict?[type.rawValue]
          if value != nil && value > 0 { translatedUpdatesDict[type] = value! }
        }
        completion(translatedUpdatesDict)
        
      } catch {
      
        completion([:])
      }
    }
    
  }
  
  
  func getDoorCatalog(completion: ([FRMDoorCatalog], NSError?)->()) {
    if let url = AppURL.Catalog.url {
      self.requestSerializer = AFJSONRequestSerializer();
      self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>;
      
      request(nil, endpoint: url.absoluteString, method: .GET, complition: { (response, error) -> () in
        if let data = response?["catalog"] as? NSArray {
          if let catalog = Mapper<FRMDoorCatalog>().mapArray(data) {
            completion(catalog, error)
            return
          }
        }
        
        completion([], error)
        
      })
    }
    
  }
  
  
  func getNews(completion: ([FRMNews], NSError?)->()) {
    if let url = AppURL.News.url {
      self.requestSerializer = AFJSONRequestSerializer();
      self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>;
      
      request(nil, endpoint: url.absoluteString, method: .GET, complition: { (response, error) -> () in
        if let data = response?["news"] as? NSArray {
          if let news = Mapper<FRMNews>().mapArray(data) {
            completion(news, error)
            return
          }
        }
        
        completion([], error)
        
      })
    }
    
  }
  
  
  func getInnovationsAndInterior(completion: ([FRMDoorInnovation], [FRMInteriorCategory], NSError?)->()) {
    if let url = AppURL.InnovationAndInterior.url {
      self.requestSerializer = AFJSONRequestSerializer();
      self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>;
      
      request(nil, endpoint: url.absoluteString, method: .GET, complition: { (response, error) -> () in
        var innovations: [FRMDoorInnovation]? = nil
        var interior: [FRMInteriorCategory]? = nil
        
        if let data = response?["doors"] as? NSArray { innovations = Mapper<FRMDoorInnovation>().mapArray(data) }
        if let data = response?["interior"] as? NSArray { interior = Mapper<FRMInteriorCategory>().mapArray(data) }
        
        completion(
          innovations != nil ? innovations! : [],
          interior != nil ? interior! : [],
          error)
        
      })
    }
    
  }
  
  
  func getElements(completion: ([FRMColor], [FRMGlass], [FRMJamb], NSError?)->()) {
    if let url = AppURL.Elements.url {
      self.requestSerializer = AFJSONRequestSerializer();
      self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>;
      
      request(nil, endpoint: url.absoluteString, method: .GET, complition: { (response, error) -> () in
        var colors: [FRMColor]? = nil
        var glasses: [FRMGlass]? = nil
        var jambs: [FRMJamb]? = nil
        
        if let data = response?["colors"] as? NSArray { colors = Mapper<FRMColor>().mapArray(data) }
        if let data = response?["glass"] as? NSArray { glasses = Mapper<FRMGlass>().mapArray(data) }
        if let data = response?["jambs"] as? NSArray { jambs = Mapper<FRMJamb>().mapArray(data) }
        
        completion(
          colors != nil ? colors! : [],
          glasses != nil ? glasses! : [],
          jambs != nil ? jambs! : [],
          error)
      
      })
    }

  }
  
  
  func getStainedGlasses(completion: ([FRMStainedGlassCategory], NSError?)->()) {
    if let url = AppURL.StainedGlasses.url {
      self.requestSerializer = AFJSONRequestSerializer();
      self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>;
      
      request(nil, endpoint: url.absoluteString, method: .GET, complition: { (response, error) -> () in
        if let data = response?["types"] as? NSArray {
          if let glasses = Mapper<FRMStainedGlassCategory>().mapArray(data) {
            completion(glasses, error)
            return
          }
        }
        
        completion([], error)
        
      })
    }
    
  }
  
  
  func getPrices(completion: ([FRMPriceList], NSError?)->()) {
    if let url = AppURL.Prices.url {
      self.requestSerializer = AFJSONRequestSerializer();
      self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>;
      
      request(nil, endpoint: url.absoluteString, method: .GET, complition: { (response, error) -> () in
        if let data = response as? NSArray {
          if let prices = Mapper<FRMPriceList>().mapArray(data) {
            completion(prices, error)
            return
          }
        }
        
        completion([], error)
        
      })
    }
    
  }
  
  
  func getShops(completion: ([FRMShopList], NSError?)->()) {
    if let url = AppURL.Shops.url {
      self.requestSerializer = AFJSONRequestSerializer();
      self.responseSerializer.acceptableContentTypes = NSSet(object: "text/html") as Set<NSObject>;
      
      request(nil, endpoint: url.absoluteString, method: .GET, complition: { (response, error) -> () in
        if let data = response?["items"] as? NSArray {
          if let shops = Mapper<FRMShopList>().mapArray(data) {
            completion(shops, error)
            return
          }
        }
        
        completion([], error)
        
      })
    }
    
  }
  
  
}
