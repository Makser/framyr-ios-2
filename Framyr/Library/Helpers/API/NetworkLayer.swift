//
//  NetworkLayer.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit



class NetworkLayer: AFHTTPRequestOperationManager {

  
  
  internal enum RequestMethod: Int {
    case GET, POST, DELETE, PUT, HEAD, PATCH
  }
  
  
  
  //  MARK: -
  //  MARK: public properties
  var memoCacheSize = 4 * 1024 * 1024;
  var diskCacheSize = 20 * 1024 * 1024;
  let DiskCacheFilePath = "/httpCache"
  
  
  var loadOnlyCached = false
  var cacheResults: Bool = false {
    didSet {
      if cacheResults == true {
        let URLCache = NSURLCache(memoryCapacity: memoCacheSize, diskCapacity: diskCacheSize, diskPath: DiskCacheFilePath);
        NSURLCache .setSharedURLCache(URLCache);
      }
      
    }
    
  }
  

  
  //  MARK: -
  //  MARK: public methods
  func request(
    data: [String: AnyObject]?,
    endpoint: String,
    method: RequestMethod,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) {
      
      self.requestSerializer.timeoutInterval = 15
      
      let parameterData = (data != nil) ? data! : [:];
      
      if loadOnlyCached {
        getCache(endpoint, completion: { (cached) -> () in
          complition?(response: cached, error: nil)
        })
        
      }
      else {
        let operation = request(
          method,
          endpoint: endpoint,
          parameters: parameterData,
          complition: complition
        );
        
        operation?.start();
        
      }
      
  }
  
  
  
  //  MARK: -
  //  MARK: private methods
  private func request(
    method: RequestMethod,
    endpoint: String,
    parameters: [String: AnyObject],
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) -> AFHTTPRequestOperation? {
      
      switch method {
        
      case .GET:
        return requestGETMethod(parameters, endpoint: endpoint, complition: complition);
      case .POST:
        return requestPOSTMethod(parameters, endpoint: endpoint, complition: complition);
      default:
        return nil;
      }
  }
  
  
  private func requestGETMethod(
    data: [String: AnyObject],
    endpoint: String,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) -> AFHTTPRequestOperation? {
      
      return GET(
        endpoint,
        parameters: data,
        success: { (operation, response) -> Void in
          
          self.callingSuccesses(.GET, endpoint: endpoint, data: data, response: response, complition: complition);
          
        },
        failure: { (operation, error) -> Void in
          
          self.callingFails(.GET, endpoint: endpoint, data: data, error: error, complition: complition);
          
      });
  }
  
  
  private func requestPOSTMethod(
    data: [String: AnyObject],
    endpoint: String,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) -> AFHTTPRequestOperation? {
      
      return POST(
        endpoint,
        parameters: data,
        success: { (operation, response) -> Void in
          
          self.callingSuccesses(.POST, endpoint: endpoint, data: data, response: response, complition: complition);
          
        },
        failure: { (operation, error) -> Void in
          
          self.callingFails(.POST, endpoint: endpoint, data: data, error: error, complition: complition);
          
      });
  }
  
  
  func requestFile(
    url: NSURL,
    completion: (NSData?, url: NSURL?)->()
    ) {
      guard let name = url.lastPathComponent else { completion(nil, url: url); return }
      
      getCache(name) { (cached) -> () in
        if let cached = cached as? NSData { completion(cached, url: url); return }
        
        let request = NSURLRequest(URL: url)
        let operation = AFHTTPRequestOperation(request: request)
        
        operation.setCompletionBlockWithSuccess(
          { (operation, responseObject) -> Void in
            self.setCache(responseObject, withKey: name)
            completion(responseObject as? NSData, url: url)
          },
          failure: { (operation, error) -> Void in
            completion(nil, url: url)
          })
        operation.start()

      }

  }
  
  
  private func callingSuccesses(
    method: RequestMethod,
    endpoint: String,
    data: [String: AnyObject]?,
    response: AnyObject?,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) {
      
      if response != nil { setCache(response!, withKey: endpoint) }
      complition?(response: response, error: nil);
  }
  
  
  private func callingFails(
    method: RequestMethod,
    endpoint: String,
    data: [String: AnyObject]?,
    error: NSError?,
    complition: ((response: AnyObject?, error: NSError?)->())?
    ) {
      
      NSLog(
        "\n\n--- ERROR: \(error?.description)" +
          "\nMETHOD: \(method.rawValue)" +
          "\nENDPOINT: \(endpoint)" +
        "\nDATA: \(data)"
      );
      
      getCache(endpoint) { (cached) -> () in
        complition?(response: cached, error: error)
      }
      
  }
  
  
  
  //  MARK: -
  //  MARK: private methods
  private let cachePath: String = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.CachesDirectory, NSSearchPathDomainMask.UserDomainMask, true).first!
  
  
  private func cachePathForKey(key: String) -> String {
    return NSURL(fileURLWithPath: cachePath).URLByAppendingPathComponent("\(NSURL(string: key)!.lastPathComponent)").absoluteString }
  
  
  private func setCache(value: AnyObject, withKey key: String) {
    let cache = NSKeyedArchiver.archivedDataWithRootObject(value)
    
    let storage = Shared.dataCache
    storage.set(value: cache, key: key)
    
    storage.fetch(key: key)
      .onSuccess { (data) -> () in
        if let _ = NSKeyedUnarchiver.unarchiveObjectWithData(data) {
          NSLog("Data for key \'\(key)\' stored to cache")
        }
      }
      .onFailure { (error) -> () in
        NSLog("Data for key \'\(key)\' not stored from cache")
    }
    
  }
  
  
  private func getCache(key: String, completion: (AnyObject?)->()) {
    
    let storage = Shared.dataCache
    storage.fetch(key: key)
      .onSuccess { (data) -> () in
        if let cache = NSKeyedUnarchiver.unarchiveObjectWithData(data) {
          NSLog("Data for key \'\(key)\' loaded from cache")
          completion(cache)
        } else {
          completion(nil)
        }
        
      }
      .onFailure { (error) -> () in
        NSLog("Data for key \'\(key)\' not loaded from cache")
        completion(nil)
      }
    
  }
  
  
}
