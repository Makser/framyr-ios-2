//
//  MessageCenter.swift
//  Framyr
//
//  Created by Admin on 14.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation



class MessageCenter {

  
  //  MARK: -
  //  MARK: properties
  static let sharedInstance = MessageCenter()
  
  
  private var mbHUD: MBProgressHUD?
  
  
  
  //  MARK: -
  //  MARK: methods
  func showProgress(title: String, progress: Double) {
    if mbHUD == nil {
      guard let window = UIApplication.sharedApplication().delegate?.window else {
        NSLog("MessageCenter.showProgress: No window")
        return }
      mbHUD = MBProgressHUD.showHUDAddedTo(window, animated: true)
      
    }
    mbHUD?.mode = .DeterminateHorizontalBar
    mbHUD?.labelText = title
    mbHUD?.progress = Float(progress)
    mbHUD?.detailsLabelText = String(format: "%.1f%%", progress * 100)
    
  }
  
  
  func showLoading(title: String) {
    if mbHUD == nil {
      guard let window = UIApplication.sharedApplication().delegate?.window else {
        NSLog("MessageCenter.showLoading: No window")
        return }
      mbHUD = MBProgressHUD.showHUDAddedTo(window, animated: true)
      
    }
    mbHUD?.mode = .Indeterminate
    mbHUD?.labelText = title
  }
  
  
  func showToast(message: String) {
    guard let window = UIApplication.sharedApplication().delegate?.window else {
      NSLog("MessageCenter.showToast: No window")
      return }
    
    window?.makeToast(message: message)
  }
  
  
  func showQuestion(title: String, message: String, buttons: [ (title: String, handler: ()->()) ]) {
    let alert = SCLAlertView()
    
    for button in buttons {
      alert.addButton(button.title, action: button.handler)
    }
    
    alert.showCloseButton = false
    alert.showTitle(
      title,
      subTitle: message,
      style: SCLAlertViewStyle.Notice,
      colorStyle: AppColors.accent.rawValue)
  }
  
  
  func hide() {
    if let hud = mbHUD { hud.hide(true); mbHUD = nil }
    
  }
  
  
  //  MARK: -
  //  MARK: element loading
  private let kElementLoadingActivityTag = 20001
  
  
  func showElementLoading(view: UIView) {
    if let _ = view.viewWithTag(kElementLoadingActivityTag) { return }
    
    let loading = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
    loading.hidesWhenStopped = true
    loading.tag = kElementLoadingActivityTag
    
    view.addSubview(loading)
    
    loading.translatesAutoresizingMaskIntoConstraints = false
    
    let xConstraint = NSLayoutConstraint(item: loading, attribute: .CenterX, relatedBy: .Equal, toItem: view, attribute: .CenterX, multiplier: 1, constant: 0)
    let yConstraint = NSLayoutConstraint(item: loading, attribute: .CenterY, relatedBy: .Equal, toItem: view, attribute: .CenterY, multiplier: 1, constant: 0)
    
    view.addConstraints([xConstraint, yConstraint])
    
    loading.startAnimating()
  }
  
  
  func hideElementLoading(view: UIView) {
    if let loading = view.viewWithTag(kElementLoadingActivityTag) as? UIActivityIndicatorView {
      loading.stopAnimating()
      loading.removeFromSuperview()
      return
    }
    
  }
  
  
}