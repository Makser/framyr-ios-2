//
//  Singletone.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation



func priceToString(price: Double) -> String {
  let formatter = NSNumberFormatter()
  formatter.numberStyle = .CurrencyStyle
  formatter.locale = NSLocale(localeIdentifier: "ru-RU")
  
  if let priceString = formatter.stringFromNumber(NSNumber(double: price)) { return priceString }
  else { return "" }

}



class Singletone {
  
  
  
  //  MARK: -
  //  MARK: properties
  static let sharedInstance = Singletone()
  
  
  var doorCatalogs = [FRMDoorCatalog]()
//    { didSet { print(doorCatalogs) } }
  
  var news = [FRMNews]()
//    { didSet { print(news) } }
  
  var doorInnovations = [FRMDoorInnovation]()
//    { didSet { print(doorInnovations) } }
  
  var interior = [FRMInteriorCategory]()
//    { didSet { print(interior) } }
  
  var colors = [FRMColor]()
//    { didSet { print(colors) } }
  
  var glasses = [FRMGlass]()
//    { didSet { print(glasses) } }
  
  var jambs = [FRMJamb]()
//    { didSet { print(jambs) } }
  
  var stainedGlasses = [FRMStainedGlassCategory]()
//    { didSet { print(stainedGlasses) } }
  
  var prices = [FRMPriceList]()
//    { didSet { print(prices) } }
  
  var shops = [FRMShopList]()
//    { didSet { print(shops) } }
  
  var basket = FRMBasket()
  
  
  private let kUserCityKey = "kUserCityKey"
  var userCityName: String {
    get { return NSUserDefaults.standardUserDefaults().objectForKey(kUserCityKey) as? String ?? "Санкт-Петербург" }
    set { NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: kUserCityKey); NSUserDefaults.standardUserDefaults().synchronize() }
  }
  
  
  var userCity: FRMShopList? {
    for shop in shops { if shop.name == userCityName { return shop } }
    return nil
  }
  
  
  private let kLastUpdateDateKey = "kLastUpdateDateKey"
  var lastUpdateDate: NSDate? {
    get { return NSUserDefaults.standardUserDefaults().objectForKey(kLastUpdateDateKey) as? NSDate }
    set {
      NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: kLastUpdateDateKey)
      NSUserDefaults.standardUserDefaults().synchronize()
    }
  }
  
  
  
  //  MARK: -
  //  MARK: data loading
  func downloadData(completion: (updated: Bool)->(), progress: (Double)->()) {
    loadData({ (updated) -> () in
      self.lastUpdateDate = NSDate()
      completion(updated: updated)
      
      },
    progress: progress,
    cached: false)
    
  }
  
  
  func loadCachedData(completion: (updated: Bool)->(), progress: (Double)->()) {
    loadData(completion,
      progress: progress,
      cached: true)
    
  }
  
  
  private func loadData(completion: (updated: Bool)->(), progress: (Double)->(), cached: Bool) {
    FRMAPILayer.sharedInstance.loadOnlyCached = cached
    
    FRMAPILayer.sharedInstance.getDoorCatalog { (catalogs, error) -> () in
    Singletone.sharedInstance.doorCatalogs = catalogs
      progress(1.0/7.0)
      
    FRMAPILayer.sharedInstance.getNews({ (news, error) -> () in
    Singletone.sharedInstance.news = news.reverse()
      progress(2.0/7.0)
      
    FRMAPILayer.sharedInstance.getInnovationsAndInterior({ (innovations, interior, error) -> () in
    Singletone.sharedInstance.doorInnovations = innovations
    Singletone.sharedInstance.interior = interior
      progress(3.0/7.0)
      
    FRMAPILayer.sharedInstance.getElements({ (colors, glasses, jambs, error) -> () in
    Singletone.sharedInstance.colors = colors
    Singletone.sharedInstance.glasses = glasses
    Singletone.sharedInstance.jambs = jambs
      progress(4.0/7.0)
      
    FRMAPILayer.sharedInstance.getStainedGlasses({ (stainedGlasses, error) -> () in
    Singletone.sharedInstance.stainedGlasses = stainedGlasses
      progress(5.0/7.0)
      
    FRMAPILayer.sharedInstance.getPrices({ (prices, error) -> () in
    Singletone.sharedInstance.prices = prices
      progress(6.0/7.0)
      
    FRMAPILayer.sharedInstance.getShops({ (shops, error) -> () in
    Singletone.sharedInstance.shops = shops
      progress(7.0/7.0)
      
      completion(updated: !FRMAPILayer.sharedInstance.loadOnlyCached)
                  
    })
                
    })
              
    })
            
    })
          
    })
        
    })
      
    }

  }
  
  
}