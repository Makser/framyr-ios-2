//
// Autogenerated by Natalie - Storyboard Generator Script.
// http://blog.krzyzanowskim.com
//

import UIKit

//MARK: - Storyboards
struct Storyboards {

    struct Main {

        static let identifier = "Main"

        static var storyboard: UIStoryboard {
            return UIStoryboard(name: self.identifier, bundle: nil)
        }

        static func instantiateInitialViewController() -> SWRevealViewController {
            return self.storyboard.instantiateInitialViewController() as! SWRevealViewController
        }

        static func instantiateViewControllerWithIdentifier(identifier: String) -> UIViewController {
            return self.storyboard.instantiateViewControllerWithIdentifier(identifier)
        }

        static func instantiateLeftSideController() -> FRMLeftSideMenuViewController {
            return self.storyboard.instantiateViewControllerWithIdentifier("LeftSideController") as! FRMLeftSideMenuViewController
        }

        static func instantiateMenuViewController() -> UINavigationController {
            return self.storyboard.instantiateViewControllerWithIdentifier("MenuViewController") as! UINavigationController
        }

        static func instantiateDoorSystemText() -> FRMDoorSystemTextViewController {
            return self.storyboard.instantiateViewControllerWithIdentifier("DoorSystemText") as! FRMDoorSystemTextViewController
        }

        static func instantiateDoorSystemGIF() -> FRMDoorSystemsGIFViewController {
            return self.storyboard.instantiateViewControllerWithIdentifier("DoorSystemGIF") as! FRMDoorSystemsGIFViewController
        }

        static func instantiateBasket() -> FRMBasketViewController {
            return self.storyboard.instantiateViewControllerWithIdentifier("Basket") as! FRMBasketViewController
        }

        static func instantiateBasketCityDialog() -> FRMBasketCityViewController {
            return self.storyboard.instantiateViewControllerWithIdentifier("BasketCityDialog") as! FRMBasketCityViewController
        }

        static func instantiateDoorSystemVideo() -> FRMDoorSystemVideoListViewController {
            return self.storyboard.instantiateViewControllerWithIdentifier("DoorSystemVideo") as! FRMDoorSystemVideoListViewController
        }
    }
}

//MARK: - ReusableKind
enum ReusableKind: String, CustomStringConvertible {
    case TableViewCell = "tableViewCell"
    case CollectionViewCell = "collectionViewCell"

    var description: String { return self.rawValue }
}

//MARK: - SegueKind
enum SegueKind: String, CustomStringConvertible {    
    case Relationship = "relationship" 
    case Show = "show"                 
    case Presentation = "presentation" 
    case Embed = "embed"               
    case Unwind = "unwind"             
    case Push = "push"                 
    case Modal = "modal"               
    case Popover = "popover"           
    case Replace = "replace"           
    case Custom = "custom"             

    var description: String { return self.rawValue } 
}

//MARK: - SegueProtocol
public protocol IdentifiableProtocol: Equatable {
    var identifier: String? { get }
}

public protocol SegueProtocol: IdentifiableProtocol {
}

public func ==<T: SegueProtocol, U: SegueProtocol>(lhs: T, rhs: U) -> Bool {
    return lhs.identifier == rhs.identifier
}

public func ~=<T: SegueProtocol, U: SegueProtocol>(lhs: T, rhs: U) -> Bool {
    return lhs.identifier == rhs.identifier
}

public func ==<T: SegueProtocol>(lhs: T, rhs: String) -> Bool {
    return lhs.identifier == rhs
}

public func ~=<T: SegueProtocol>(lhs: T, rhs: String) -> Bool {
    return lhs.identifier == rhs
}

public func ==<T: SegueProtocol>(lhs: String, rhs: T) -> Bool {
    return lhs == rhs.identifier
}

public func ~=<T: SegueProtocol>(lhs: String, rhs: T) -> Bool {
    return lhs == rhs.identifier
}

//MARK: - ReusableViewProtocol
public protocol ReusableViewProtocol: IdentifiableProtocol {
    var viewType: UIView.Type? { get }
}

public func ==<T: ReusableViewProtocol, U: ReusableViewProtocol>(lhs: T, rhs: U) -> Bool {
    return lhs.identifier == rhs.identifier
}

//MARK: - Protocol Implementation
extension UIStoryboardSegue: SegueProtocol {
}

extension UICollectionReusableView: ReusableViewProtocol {
    public var viewType: UIView.Type? { return self.dynamicType }
    public var identifier: String? { return self.reuseIdentifier }
}

extension UITableViewCell: ReusableViewProtocol {
    public var viewType: UIView.Type? { return self.dynamicType }
    public var identifier: String? { return self.reuseIdentifier }
}

//MARK: - UIViewController extension
extension UIViewController {
    func performSegue<T: SegueProtocol>(segue: T, sender: AnyObject?) {
        if let identifier = segue.identifier {
            performSegueWithIdentifier(identifier, sender: sender)
        }
    }

    func performSegue<T: SegueProtocol>(segue: T) {
        performSegue(segue, sender: nil)
    }
}

//MARK: - UICollectionView

extension UICollectionView {

    func dequeueReusableCell<T: ReusableViewProtocol>(reusable: T, forIndexPath: NSIndexPath!) -> UICollectionViewCell? {
        if let identifier = reusable.identifier {
            return dequeueReusableCellWithReuseIdentifier(identifier, forIndexPath: forIndexPath)
        }
        return nil
    }

    func registerReusableCell<T: ReusableViewProtocol>(reusable: T) {
        if let type = reusable.viewType, identifier = reusable.identifier {
            registerClass(type, forCellWithReuseIdentifier: identifier)
        }
    }

    func dequeueReusableSupplementaryViewOfKind<T: ReusableViewProtocol>(elementKind: String, withReusable reusable: T, forIndexPath: NSIndexPath!) -> UICollectionReusableView? {
        if let identifier = reusable.identifier {
            return dequeueReusableSupplementaryViewOfKind(elementKind, withReuseIdentifier: identifier, forIndexPath: forIndexPath)
        }
        return nil
    }

    func registerReusable<T: ReusableViewProtocol>(reusable: T, forSupplementaryViewOfKind elementKind: String) {
        if let type = reusable.viewType, identifier = reusable.identifier {
            registerClass(type, forSupplementaryViewOfKind: elementKind, withReuseIdentifier: identifier)
        }
    }
}
//MARK: - UITableView

extension UITableView {

    func dequeueReusableCell<T: ReusableViewProtocol>(reusable: T, forIndexPath: NSIndexPath!) -> UITableViewCell? {
        if let identifier = reusable.identifier {
            return dequeueReusableCellWithIdentifier(identifier, forIndexPath: forIndexPath)
        }
        return nil
    }

    func registerReusableCell<T: ReusableViewProtocol>(reusable: T) {
        if let type = reusable.viewType, identifier = reusable.identifier {
            registerClass(type, forCellReuseIdentifier: identifier)
        }
    }

    func dequeueReusableHeaderFooter<T: ReusableViewProtocol>(reusable: T) -> UITableViewHeaderFooterView? {
        if let identifier = reusable.identifier {
            return dequeueReusableHeaderFooterViewWithIdentifier(identifier)
        }
        return nil
    }

    func registerReusableHeaderFooter<T: ReusableViewProtocol>(reusable: T) {
        if let type = reusable.viewType, identifier = reusable.identifier {
             registerClass(type, forHeaderFooterViewReuseIdentifier: identifier)
        }
    }
}


//MARK: - SWRevealViewController
extension UIStoryboardSegue {
    func selection() -> SWRevealViewController.Segue? {
        if let identifier = self.identifier {
            return SWRevealViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension SWRevealViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case swrear = "sw_rear"
        case swfront = "sw_front"

        var kind: SegueKind? {
            switch (self) {
            case swrear:
                return SegueKind(rawValue: "custom")
            case swfront:
                return SegueKind(rawValue: "custom")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case swrear:
                return UINavigationController.self
            case swfront:
                return FRMStartViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}

//MARK: - FRMStartViewController

//MARK: - FRMLeftSideMenuViewController
extension UIStoryboardSegue {
    func selection() -> FRMLeftSideMenuViewController.Segue? {
        if let identifier = self.identifier {
            return FRMLeftSideMenuViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMLeftSideMenuViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueMenuToCollections = "segueMenuToCollections"
        case segueMenuToInnovations = "segueMenuToInnovations"
        case segueMenuToInterior = "segueMenuToInterior"
        case segueMenuToNews = "segueMenuToNews"
        case segueMenuToContact = "segueMenuToContact"
        case segueMenuToMap = "segueMenuToMap"

        var kind: SegueKind? {
            switch (self) {
            case segueMenuToCollections:
                return SegueKind(rawValue: "custom")
            case segueMenuToInnovations:
                return SegueKind(rawValue: "custom")
            case segueMenuToInterior:
                return SegueKind(rawValue: "custom")
            case segueMenuToNews:
                return SegueKind(rawValue: "custom")
            case segueMenuToContact:
                return SegueKind(rawValue: "custom")
            case segueMenuToMap:
                return SegueKind(rawValue: "custom")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueMenuToCollections:
                return UINavigationController.self
            case segueMenuToInnovations:
                return UINavigationController.self
            case segueMenuToInterior:
                return UINavigationController.self
            case segueMenuToNews:
                return UINavigationController.self
            case segueMenuToContact:
                return UINavigationController.self
            case segueMenuToMap:
                return UINavigationController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMLeftSideMenuViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMLeftSideTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMCollectionListViewController
extension UIStoryboardSegue {
    func selection() -> FRMCollectionListViewController.Segue? {
        if let identifier = self.identifier {
            return FRMCollectionListViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMCollectionListViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueCollectionToDoors = "segueCollectionToDoors"

        var kind: SegueKind? {
            switch (self) {
            case segueCollectionToDoors:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueCollectionToDoors:
                return FRMDoorListViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMCollectionListViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cellOld = "cellOld"
        case cellNew = "cellNew"

        var kind: ReusableKind? {
            switch (self) {
            case cellOld:
                return ReusableKind(rawValue: "tableViewCell")
            case cellNew:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cellOld:
                return FRMCollectionTableViewCell.self
            case cellNew:
                return FRMCollectionTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMDoorSystemsListViewController
extension UIStoryboardSegue {
    func selection() -> FRMDoorSystemsListViewController.Segue? {
        if let identifier = self.identifier {
            return FRMDoorSystemsListViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMDoorSystemsListViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueListToDetail = "segueListToDetail"

        var kind: SegueKind? {
            switch (self) {
            case segueListToDetail:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueListToDetail:
                return FRMDoorSystemDetailViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMDoorSystemsListViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMInnovationTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMDoorListViewController
extension FRMDoorListViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "collectionViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMDoorCollectionViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMInteriorCategoryListViewController
extension UIStoryboardSegue {
    func selection() -> FRMInteriorCategoryListViewController.Segue? {
        if let identifier = self.identifier {
            return FRMInteriorCategoryListViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMInteriorCategoryListViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueCategoryToSubcategory = "segueCategoryToSubcategory"

        var kind: SegueKind? {
            switch (self) {
            case segueCategoryToSubcategory:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueCategoryToSubcategory:
                return FRMInteriorSubcategoryViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMInteriorCategoryListViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMInteriorCategoryTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMDoorSystemDetailViewController
extension UIStoryboardSegue {
    func selection() -> FRMDoorSystemDetailViewController.Segue? {
        if let identifier = self.identifier {
            return FRMDoorSystemDetailViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMDoorSystemDetailViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueDetailToGifList = "segueDetailToGifList"

        var kind: SegueKind? {
            switch (self) {
            case segueDetailToGifList:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueDetailToGifList:
                return FRMDoorSystemsGIFViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMDoorSystemDetailViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMInnovationDetailTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMDoorSystemTextViewController

//MARK: - FRMDoorSystemsGIFViewController
extension FRMDoorSystemsGIFViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"
        case cellZoom = "cellZoom"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "collectionViewCell")
            case cellZoom:
                return ReusableKind(rawValue: "collectionViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMInnovationGIFCollectionViewCell.self
            case cellZoom:
                return FRMInnovationGIFCollectionViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMInteriorSubcategoryViewController
extension UIStoryboardSegue {
    func selection() -> FRMInteriorSubcategoryViewController.Segue? {
        if let identifier = self.identifier {
            return FRMInteriorSubcategoryViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMInteriorSubcategoryViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueSubcategoryToList = "segueSubcategoryToList"

        var kind: SegueKind? {
            switch (self) {
            case segueSubcategoryToList:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueSubcategoryToList:
                return FRMInteriorListViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMInteriorSubcategoryViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"
        case cellGlass = "cellGlass"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            case cellGlass:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMInteriorSubcategoryTableViewCell.self
            case cellGlass:
                return FRMInteriorSubcategoryTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMInteriorListViewController
extension UIStoryboardSegue {
    func selection() -> FRMInteriorListViewController.Segue? {
        if let identifier = self.identifier {
            return FRMInteriorListViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMInteriorListViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueListToDetail = "segueListToDetail"

        var kind: SegueKind? {
            switch (self) {
            case segueListToDetail:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueListToDetail:
                return FRMInteriorDetailViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMInteriorListViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case header = "header"
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case header:
                return ReusableKind(rawValue: "collectionViewCell")
            case cell:
                return ReusableKind(rawValue: "collectionViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case header:
                return FRMInteriorHeaderCollectionViewCell.self
            case cell:
                return FRMInteriorCollectionViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMInteriorDetailViewController
extension UIStoryboardSegue {
    func selection() -> FRMInteriorDetailViewController.Segue? {
        if let identifier = self.identifier {
            return FRMInteriorDetailViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMInteriorDetailViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueDetailToZoom = "segueDetailToZoom"
        case segueDetailToBasket = "segueDetailToBasket"

        var kind: SegueKind? {
            switch (self) {
            case segueDetailToZoom:
                return SegueKind(rawValue: "show")
            case segueDetailToBasket:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueDetailToZoom:
                return FRMZoomingViewController.self
            case segueDetailToBasket:
                return FRMBasketViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}

//MARK: - FRMZoomingViewController

//MARK: - FRMBasketViewController
extension FRMBasketViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMBasketTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMBasketCityViewController
extension FRMBasketCityViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            default:
                return nil
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMNewsListViewController
extension UIStoryboardSegue {
    func selection() -> FRMNewsListViewController.Segue? {
        if let identifier = self.identifier {
            return FRMNewsListViewController.Segue(rawValue: identifier)
        }
        return nil
    }
}

extension FRMNewsListViewController { 

    enum Segue: String, CustomStringConvertible, SegueProtocol {
        case segueListToDetail = "segueListToDetail"

        var kind: SegueKind? {
            switch (self) {
            case segueListToDetail:
                return SegueKind(rawValue: "show")
            }
        }

        var destination: UIViewController.Type? {
            switch (self) {
            case segueListToDetail:
                return FRMNewsDetailViewController.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}
extension FRMNewsListViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMNewsTableViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMNewsDetailViewController

//MARK: - FRMContactViewController

//MARK: - FRMMapViewController
extension FRMMapViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case menuCell = "menuCell"
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case menuCell:
                return ReusableKind(rawValue: "tableViewCell")
            case cell:
                return ReusableKind(rawValue: "tableViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            default:
                return nil
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}


//MARK: - FRMDoorSystemVideoListViewController
extension FRMDoorSystemVideoListViewController { 

    enum Reusable: String, CustomStringConvertible, ReusableViewProtocol {
        case cell = "cell"

        var kind: ReusableKind? {
            switch (self) {
            case cell:
                return ReusableKind(rawValue: "collectionViewCell")
            }
        }

        var viewType: UIView.Type? {
            switch (self) {
            case cell:
                return FRMInnovationVideoCollectionViewCell.self
            }
        }

        var identifier: String? { return self.description } 
        var description: String { return self.rawValue }
    }

}

