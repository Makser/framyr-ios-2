//
//  Constants.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright (c) 2015 sema. All rights reserved.
//

import Foundation



enum AppURL: String {
  case Domain = "http://framyr.ru"
  case Catalog = "/api/catalog.php"
  case News = "/api/news.php"
  case InnovationAndInterior = "/api/descriptions.php"
  case Elements = "/api/elements.php"
  case StainedGlasses = "/api/glasses.php"
  case Prices = "/api/prices.php"
  case Shops = "/api/shops.php"
  case UpdateDate = "/api/update.php"
  case UpdateList = "/api/edit.php"
  
  
  var stringURL: String { return self.rawValue }
  
  
  var url: NSURL? {
    switch self {
    case .Domain: return NSURL(string: stringURL)
    default: return AppURL.Domain.url?.URLByAppendingPathComponent(stringURL)
    }
    
  }
  
  
}



enum AppColors: UInt {
  case color0 = 0x35383d
  case color1 = 0x3e4147
  case color2 = 0x757575
  case color3 = 0xa7a7a2
  
  case accent0 = 0x006E2A
  case accent = 0x009639
  
  
  case white = 0xFFFFFF
  
  
  var color: UIColor {
    return UIColor(
      red: CGFloat((self.rawValue >> 16) & 0xFF) / 255.0,
      green: CGFloat((self.rawValue >> 8) & 0xFF) / 255.0,
      blue: CGFloat((self.rawValue >> 0) & 0xFF) / 255.0,
      alpha: 1.0)
  }
  
  
}



enum AppFonts: String {
  case HelveticaNeueCyrLight = "HelveticaNeueCyr-Light"
  case HelveticaNeueCyrRoman = "HelveticaNeueCyr-Roman"
  
  
  func font(size: CGFloat) -> UIFont? { return UIFont(name: rawValue, size: size) }
  

}