//
//  IcomoonExtensions.swift
//  Framyr
//
//  Created by Admin on 26.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


enum Icomoon: String {
  case Adv1 = "\u{E600}"
  case Adv2 = "\u{E601}"
  case Adv3 = "\u{E602}"
  case Adv4 = "\u{E603}"
  case Adv5 = "\u{E604}"
  case Adv6 = "\u{E605}"
  case Clock = "\u{E606}"
  case IPhone = "\u{E607}"
  case VK = "\u{E608}"
  case Arch = "\u{E609}"
  case ArrowDown = "\u{E60a}"
  case ArrowLeft = "\u{E60b}"
  case ArrowRight = "\u{E60c}"
  case ArrowUp = "\u{E60d}"
  case Back = "\u{E60e}"
  case Close = "\u{E60f}"
  case Color = "\u{E610}"
  case Date = "\u{E611}"
  case Decor = "\u{E612}"
  case Door = "\u{E613}"
  case Filter = "\u{E614}"
  case City = "\u{E615}"
  case Jamb = "\u{E616}"
  case Measure = "\u{E617}"
  case Menu = "\u{E618}"
  case Mezzanine = "\u{E619}"
  case Name = "\u{E61a}"
  case Phone = "\u{E61b}"
  case Photo = "\u{E61c}"
  case PhotoPoint = "\u{E61d}"
  case Reset = "\u{E61e}"
  case Save = "\u{E61f}"
  case Select = "\u{E620}"
  case Shop = "\u{E621}"
  case StainedGlass = "\u{E622}"
  case Text = "\u{E623}"
  case Window = "\u{E624}"
  
  
  var string: NSAttributedString {
    return NSAttributedString(string: rawValue, attributes: [
      NSFontAttributeName : UIFont(name: "icomoon", size: 40)!
      ])
  }
  
}


extension UILabel {
  
  
  var icon: Icomoon? {
    get { return nil }
    set {
      guard let icon = newValue else { text = ""; return }
      
      font = UIFont(name: "icomoon", size: font.pointSize)
      text = icon.rawValue
    }
    
  }
  
  
}


extension UIButton {

  
  var icon: Icomoon? {
    get { return nil }
    set {
      guard let icon = newValue else { setTitle("", forState: .Normal); return }
      
      titleLabel?.font = UIFont(name: "icomoon", size: titleLabel?.font.pointSize ?? 20)
      setTitle(icon.rawValue, forState: .Normal)
    }
  }
  
  
}
