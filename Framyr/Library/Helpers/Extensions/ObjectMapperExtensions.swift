//
//  ObjectMapperExtensions.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation



let intTransform = TransformOf<Int, String>(fromJSON: { $0?.toInt() }, toJSON: { $0.map { String($0) } })



let intArrayTransform = TransformOf<[Int], [String]>(
  fromJSON: { (stringArrayValue) -> [Int]? in
    stringArrayValue?.reduce([Int]()) { (res, value: String) -> [Int] in
      if let intValue = Int(value) { return res + [intValue] }
      else { return res } }
    },
  toJSON: { (intArrayValue) -> [String]? in
    intArrayValue?.reduce([String]()) { (res, value: Int) -> [String] in
      return res + ["\(value)"]
    }
  })



let doubleTransform = TransformOf<Double, String>(fromJSON: { $0?.toDouble() }, toJSON: { $0.map { String($0) } })



let intDoubleArrayTransform = TransformOf<[Int: Double], [NSDictionary]>(
  fromJSON: { (value: [NSDictionary]?) -> [Int : Double]? in

    return value?.reduce(
      [Int: Double](),
      combine: { (res: [Int : Double], value: NSDictionary) -> [Int : Double] in
        return value.reduce(res, combine: { (var res: [Int : Double], val: (key: AnyObject, value: AnyObject)) -> [Int : Double] in
          var k: Int? = nil
          var v: Double? = nil
          
          switch val.key {
          case let sKey as String: k = sKey.toInt()
          case let iKey as Int: k = iKey
          default: break
          }
          
          switch val.value {
          case let sValue as String: v = sValue.toDouble()
          case let dValue as Double: v = dValue
          default: break
          }
          
          if let iKey = k, let dValue = v { res[iKey] = dValue }
      
          return res
        })
    })
    
  },
  toJSON: { (value: [Int : Double]?) -> [NSDictionary]? in
    return []
})



private let newsDateFormat = "dd.MM.yyyy HH:mm:ss"
let dateTransform = TransformOf<NSDate, String>(
  fromJSON: { (value: String?) -> NSDate? in
    if let stringValue = value {
      let dateFormatter = NSDateFormatter()
      dateFormatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
      dateFormatter.dateFormat = newsDateFormat
      
      return dateFormatter.dateFromString(stringValue) }
    else {
      return nil }
  
  }) { (date: NSDate?) -> String? in
    if let dateValue = date {
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = newsDateFormat
      return dateFormatter.stringFromDate(dateValue) }
    else {
      return nil
    }

  }



  