//
//  UIImageViewExtensions.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


extension UIImageView {


  func setImageURL(url: NSURL?, completion: (()->())? = nil) {
    guard let url = url else { return }
    
    MessageCenter.sharedInstance.showElementLoading(self)
    sd_setImageWithURL(url, placeholderImage: nil) { (_, _, _, receivedURL) -> Void in
      if receivedURL != url { self.image = nil }
      else {
        completion?()
        MessageCenter.sharedInstance.hideElementLoading(self) }
      
    }
    
  }
  
  
}