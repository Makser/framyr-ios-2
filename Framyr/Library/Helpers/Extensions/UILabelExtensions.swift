//
//  UILabelExtensions.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


extension UILabel {


  var fontSize: CGFloat {
    get { return font.pointSize }
    set { font = font.fontWithSize(newValue) }
  }
  
  
}