//
//  UIViewExtensions.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


private let kGridViewTag = 20002


extension UIView {


  func addGrid() {
    if let _ = viewWithTag(kGridViewTag) { return }
    
    let gridView = UIView(frame: self.bounds)
    gridView.tag = kGridViewTag
    
    let gridColor = UIColor(patternImage: UIImage(named: "grid")!)
    gridView.backgroundColor = gridColor
    gridView.userInteractionEnabled = false
    
    addSubview(gridView)
    
    gridView.translatesAutoresizingMaskIntoConstraints = false
    
    let xConstraint = NSLayoutConstraint(item: gridView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
    let yConstraint = NSLayoutConstraint(item: gridView, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
    let widthConstraint = NSLayoutConstraint(item: gridView, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1, constant: 0)
    let heightConstraint = NSLayoutConstraint(item: gridView, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: 0)
    
    addConstraints([xConstraint, yConstraint, widthConstraint, heightConstraint])

    sendSubviewToBack(gridView)
  }
  
  
  func removeGrid() {
    if let view = viewWithTag(kGridViewTag) {
      view.removeFromSuperview()
    }
    
  }
  
  
}