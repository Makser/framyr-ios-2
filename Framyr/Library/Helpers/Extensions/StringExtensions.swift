//
//  StringExtensions.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


extension String {

  
  func toInt() -> Int? { return Int(self) }
  
  
  func toDouble() -> Double? { return Double(self) }
  
  
  func shortString(length: Int) -> String { return substringToIndex(startIndex.advancedBy(max(0, min(characters.count - 1, length)))) }
  
  
  func html2AttributedString() -> NSAttributedString? {
    guard let data = dataUsingEncoding(NSUTF8StringEncoding) else { return nil }
  
    do { return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute:NSUTF8StringEncoding], documentAttributes: nil) }
    catch let error as NSError {
      return nil
    }
  }
  
  
  func html2String() -> String {
    return html2AttributedString()?.string ?? ""
  }
  
  
}