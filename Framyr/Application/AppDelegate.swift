//
//  AppDelegate.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright (c) 2015 sema. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
//  var sideMenuController: LGSideMenuController?

  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

//    let initialNavigationController = Storyboards.Main.instantiateInitialViewController()
//    
//    sideMenuController = LGSideMenuController(rootViewController: initialNavigationController)
//    sideMenuController?.setLeftViewEnabledWithWidth(200, presentationStyle: LGSideMenuPresentationStyle.ScaleFromBig, alwaysVisibleOptions: LGSideMenuAlwaysVisibleOptions.OnNone)
//    
//    let leftSideMenuController = Storyboards.Main.instantiateLeftSideController()
//    sideMenuController?.leftView().addSubview(leftSideMenuController.view)
//    
//    window?.rootViewController = sideMenuController
//    window?.makeKeyAndVisible()
    
    //  sdwebimage
    SDWebImageManager.sharedManager().imageDownloader.executionOrder = SDWebImageDownloaderExecutionOrder.LIFOExecutionOrder
    SDWebImageManager.sharedManager().imageDownloader.maxConcurrentDownloads = 3
    

    return true
  }


}

