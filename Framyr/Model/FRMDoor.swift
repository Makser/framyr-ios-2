//
//  FRMDoor.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMDoor: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var doorID = 0
  var glassIDs = [Int]()
  var photoUrl = ""
  var patternDoorUrl = ""
  var patternGlassUrl = ""
  var patternTextureUrl = ""
  var price = 0.0
  var sort = 0
  
  
  var photoFullURL: NSURL? { return AppURL.Domain.url?.URLByAppendingPathComponent(photoUrl) }
  
  
  override var description: String {
    return "\n\nDoor\n" +
    "name: \(name)\n" +
    "id: \(doorID)\n" +
    "glasses: \(glassIDs)\n" +
    "photo: \(photoUrl)\n" +
    "p_door_url: \(patternDoorUrl)\n" +
    "p_glass_url: \(patternGlassUrl)\n" +
    "p_text_url: \(patternTextureUrl)\n" +
    "price: \(price)\n" +
    "sort: \(sort)"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name                <- map["name"]
    doorID              <- (map["id"], intTransform)
    glassIDs            <- (map["glass"], intArrayTransform)
    photoUrl            <- map["photo_url"]
    patternDoorUrl      <- map["pattern_door_url"]
    patternGlassUrl     <- map["pattern_glass_url"]
    patternTextureUrl   <- map["pattern_texture_url"]
    price               <- (map["price"], doubleTransform)
    sort                <- (map["sort"], intTransform)
  }
  
  
}
