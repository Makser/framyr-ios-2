//
//  FRMStainedGlassCategory.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMStainedGlassCategory: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var url = ""
  var name = ""
  var descr = ""
  var glasses = [FRMStainedGlass]()
  
  
  var fullUrl: NSURL? { return AppURL.Domain.url?.URLByAppendingPathComponent(url) }
  
  
  override var description: String {
    return "\n\nSt. glass category\n" +
    "name: \(name)\n" +
    "url: \(url)\n" +
    "descr: \(descr.shortString(10))...\n" +
    "glasses: \(glasses)"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    url       <- map["url"]
    name      <- map["name"]
    descr     <- map["description"]
    glasses   <- map["images"]
    
    descr = descr.html2String()
  }
  
  
}
