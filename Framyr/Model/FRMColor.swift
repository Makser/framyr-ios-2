//
//  FRMColor.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMColor: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var colorID = 0
  var coveringType = [Int]()
  var name = ""
  var url = ""
  var sort = 0
  
  
  override var description: String {
    return "\n\nColor\n" +
    "id: \(colorID)\n" +
    "cov. type: \(coveringType)\n" +
    "name: \(name)\n" +
    "url: \(url)\n" +
    "sort: \(sort)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    colorID         <- (map["id"], intTransform)
    coveringType    <- (map["covering_type"], intArrayTransform)
    name            <- map["name"]
    url             <- map["url"]
    sort            <- (map["sort"], intTransform)
  }
  
  
}
