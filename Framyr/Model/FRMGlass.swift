//
//  FRMGlass.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMGlass: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var glassID = 0
  var name = ""
  var price = 0.0
  var url = ""
  
  
  override var description: String {
    return "\n\nGlass\n" +
    "id: \(glassID)\n" +
    "name: \(name)\n" +
    "price: \(price)\n" +
    "url: \(url)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    glassID     <- (map["id"], intTransform)
    name        <- map["name"]
    price       <- (map["price"], doubleTransform)
    url         <- map["url"]
  }
  
  
}
