//
//  FRMCatalog.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation



class FRMDoorCatalog: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var collections = [FRMDoorCollection]()
  
  
  override var description: String {
    return "\n\nCatalog\n" +
    "name: \(name)\n" +
    "collections: \n\(collections)"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name          <- map["name"]
    collections   <- map["collections"]
  }
  
  
}