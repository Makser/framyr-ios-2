//
//  FRMGoods.swift
//  Framyr
//
//  Created by Admin on 27.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


class FRMGoods: NSObject, NSCoding {

  
  
  //  MARK: -
  //  MARK: properties
  var imageUrl = ""
  var name = ""
  var price = 0.0
  var goodsId = 0
  var count = 0
  var totalPrice: Double { return Double(count) * price }
  
  
  var fullURL: NSURL? { return imageUrl != "" ? AppURL.Domain.url?.URLByAppendingPathComponent(imageUrl) : nil }
  
  
  
  //  MARK: -
  //  MARK: methods
  init(interior: FRMinterior) {
    imageUrl = interior.url
    name = interior.name
    price = interior.price
    goodsId = interior.interiorID
    count = 1
  }
  
  
  init(glass: FRMStainedGlass) {
    imageUrl = glass.url
    name = glass.name
    price = glass.price
    goodsId = glass.glassID
    count = 1
  }
  
  
  
  //  MARK: -
  //  MARK: NSCoding
  private enum CodingKey : String {
    case Url = "kUrl"
    case Name = "kName"
    case Price = "kPrice"
    case Count = "kCount"
    case ID = "kID"
  }
  
  required init?(coder aDecoder: NSCoder) {
    imageUrl = aDecoder.decodeObjectForKey(CodingKey.Url.rawValue) as? String ?? ""
    name = aDecoder.decodeObjectForKey(CodingKey.Name.rawValue) as? String ?? ""
    price = aDecoder.decodeObjectForKey(CodingKey.Price.rawValue) as? Double ?? 0.0
    goodsId = aDecoder.decodeObjectForKey(CodingKey.ID.rawValue) as? Int ?? 0
    count = aDecoder.decodeObjectForKey(CodingKey.Count.rawValue) as? Int ?? 1
  }
  
  
  func encodeWithCoder(aCoder: NSCoder) {
    aCoder.encodeObject(imageUrl, forKey: CodingKey.Url.rawValue)
    aCoder.encodeObject(name, forKey: CodingKey.Name.rawValue)
    aCoder.encodeObject(price, forKey: CodingKey.Price.rawValue)
    aCoder.encodeObject(goodsId, forKey: CodingKey.ID.rawValue)
    aCoder.encodeObject(count, forKey: CodingKey.Count.rawValue)
  }
  
  
}