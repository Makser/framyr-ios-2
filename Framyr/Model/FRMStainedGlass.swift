//
//  FRMStainedGlass.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMStainedGlass: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var url = ""
  var price = 0.0
  var glassID = 0
  
  
  var fullUrl: NSURL? { return AppURL.Domain.url?.URLByAppendingPathComponent(url) }
  
  
  override var description: String {
    return "\n\nSt. glass\n" +
    "name: \(name)\n" +
    "url: \(url)\n" +
    "price: \(price)\n" +
    "id: \(glassID)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name      <- map["name"]
    url       <- map["url"]
    price     <- (map["price"], doubleTransform)
    glassID   <- (map["id"], intTransform)
    
  }
  
  
}
