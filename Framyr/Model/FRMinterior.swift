//
//  FRMinterior.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMinterior: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var url = ""
  var price = 0.0
  var descr = ""
  var interiorID = 0
  
  
  var fullURL: NSURL? { return AppURL.Domain.url?.URLByAppendingPathComponent(url) }
  
  
  override var description: String {
    return "\n\nInterior\n" +
    "name: \(name)\n" +
    "url: \(url)\n" +
    "price: \(price)\n" +
    "descr: \(descr.shortString(10))...\n" +
    "id: \(interiorID)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name        <- map["name"]
    url         <- map["url"]
    price       <- (map["price"], doubleTransform)
    descr       <- map["description"]
    interiorID  <- (map["id"], intTransform)
    
    descr = descr.html2String()
  }
  
  
}
