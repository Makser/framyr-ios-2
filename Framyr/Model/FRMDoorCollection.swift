//
//  FRMDoorCollection.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation



class FRMDoorCollection: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var coveringType = 0
  var descr = ""
  var doors = [FRMDoor]()
  
  
  var image: UIImage? {
    var imageName = ""
    switch name.lowercaseString {
      case "покрытие ламинат": imageName = "framyr_laminat"
      case "покрытие шпон": imageName = "framyr_shpon"
      case "покрытие экологический шпон": imageName = "framyr_eco_shpon"
      case "коллекция шпон": imageName = "fineza_shpon"
      case "коллекция эмаль-глосс": imageName = "fineza_emal_gloss"
      case "коллекция aluform": imageName = "fineza_aluform"
      case "искусственное": imageName = "framyr_eco_shpon"
      case "натуральное": imageName = "framyr_shpon"
    default: break
    }
    
    return UIImage(named: imageName)
  }
  
  
  var titleImage: UIImage? {
    var imageName = ""
    switch name.lowercaseString {
    case "покрытие ламинат": imageName = "laminat_pl"
    case "покрытие шпон": imageName = "shpon_pl"
    case "покрытие экологический шпон": imageName = "eco_shpon_pl"
    case "коллекция шпон": imageName = "shpon_pl"
    case "коллекция эмаль-глосс": imageName = "emal_gloss_pl"
    case "коллекция aluform": imageName = "aluform_pl"
    case "искусственное": imageName = "artificial_pl"
    case "натуральное": imageName = "natural_pl"
    default: break
    }
    
    return UIImage(named: imageName)
  }
  
  
  override var description: String {
    return "\n\nCollection\n" +
    "name: \(name)\n" +
    "cov_type: \(coveringType)\n" +
    "descr: \(descr.shortString(10))...\n" //+
//    "doors: \n\(doors)"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name            <- map["name"]
    coveringType    <- (map["covering_type"], intTransform)
    descr           <- map["description"]
    doors           <- map["models"]
  }
  
  
}