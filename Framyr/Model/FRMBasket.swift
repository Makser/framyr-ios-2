//
//  FRMBasket.swift
//  Framyr
//
//  Created by Admin on 27.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation


class FRMBasket {
  
  
  
  //  MARK: -
  //  MARK: properties
  private let kBasketStoreKey = "kBasketStoreKey"
  
  
  private var goods = [Int : FRMGoods] () {
    didSet { save() }
  }
  
  
  var totalPrice: Double { return getGoods().reduce(0.0, combine: { $0 + $1.totalPrice }) }
  
  
  var totalCount: Int { return getGoods().reduce(0, combine: { $0 + $1.count }) }
  
  
  //  MARK: -
  //  MARK: methods
  init() {
    load()
  }
  
  
  func addInterior(interior: FRMinterior) {
    let newGoods = FRMGoods(interior: interior)
    addGoods(newGoods)
  }
  
  
  func addGlass(glass: FRMStainedGlass) {
    let newGoods = FRMGoods(glass: glass)
    addGoods(newGoods)
  }
  
  
  func addGoods(newGoods: FRMGoods) {
    let goodsId = newGoods.goodsId
    
    if let oldGoods = goods[goodsId] {
      oldGoods.count += newGoods.count
      goods[goodsId] = oldGoods }
    else {
      goods[goodsId] = newGoods }
    
    save()
  }
  
  
  func removeGoods(goodsId: Int) {
    goods[goodsId] = nil
    save()
  }
  
  
  func getGoods() -> [FRMGoods] {
    return Array(goods.values)
  }
  
  
  func editGoods(newGoods: FRMGoods) {
    let goodsId = newGoods.goodsId
    
    if let _ = goods[goodsId] {
      goods[goodsId] = newGoods }
    
    save()
  }
  
  
  private func save() {
    let goodsData = NSKeyedArchiver.archivedDataWithRootObject(goods)
    Shared.dataCache.set(value: goodsData, key: kBasketStoreKey)
  }
  
  
  private func load() {
    Shared.dataCache.fetch(key: kBasketStoreKey)
    .onSuccess { (goodsData) -> () in
      self.goods = NSKeyedUnarchiver.unarchiveObjectWithData(goodsData) as? [Int:FRMGoods] ?? [:]
    }
  }
  
  
}