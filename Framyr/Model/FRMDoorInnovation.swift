//
//  FRMDoorInnovation.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMDoorInnovation: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var shortDescr = ""
  var descr = ""
  var images = [FRMMediaFile]()
  var video = [FRMMediaFile]()
  var gif = [FRMMediaFile]()
  
  
  var previewFullURL: NSURL? {
    if images.count == 0 { return nil }
    return AppURL.Domain.url?.URLByAppendingPathComponent(images.first!.url)
  }
  
  
  override var description: String {
    return "\n\ninnovation\n" +
    "name: \(name)\n" +
    "short_descr: \(shortDescr.shortString(10))...\n" +
    "descr: \(descr.shortString(10))...\n" +
    "images: \(images)\n" +
    "video: \(video)\n" +
    "gif: \(gif)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name        <- map["name"]
    shortDescr  <- map["short_description"]
    descr       <- map["description"]
    images      <- map["images"]
    video       <- map["video"]
    gif         <- map["gif"]
    
    descr = descr.html2String()
  }
  
}
