//
//  FRMShopList.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMShopList: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var listID = 0
  var shops = [FRMShop]()
  
  
  override var description: String {
    return "\n\nShop list\n" +
    "name: \(name)\n" +
    "id: \(listID)\n" +
    "shops: \(shops)\n"
  }
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name    <- map["name"]
    listID  <- (map["id"], intTransform)
    shops   <- map["shops"]
  }
  
  
}
