//
//  FRMPriceList.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMPriceList: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var type = ""
  var city = ""
  var prices = [Int: Double]()
  
  
  override var description: String {
    return "\n\nPriceList\n" +
    "type: \(type)\n" +
    "city: \(city)\n" +
    "prices: \(prices)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    type    <- map["type"]
    city    <- map["city"]
    prices  <- (map["prices"], intDoubleArrayTransform)
  }
  
  
}
