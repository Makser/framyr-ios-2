//
//  FRMMediaFile.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMMediaFile: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var url = ""
  
  
  var fullURL: NSURL? { return AppURL.Domain.url?.URLByAppendingPathComponent(url) }
  
  
  override var description: String {
    return "\n\nMedia\n" +
    "name: \(name)\n" +
    "url: \(url)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name  <- map["name"]
    url   <- map["url"]
  }
  
  
}
