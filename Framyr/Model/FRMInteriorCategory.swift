//
//  FRMinteriorCategory.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInteriorCategory: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var name = ""
  var items = [FRMinterior]()
  
  
  var icon: Icomoon? {
    switch name {
      case "Арки": return Icomoon.Arch
      case "Антресоли": return Icomoon.Mezzanine
      case "Витражи": return Icomoon.StainedGlass
    default: return nil
    }
  }
  
  
  override var description: String {
    return "\n\nInt. category\n" +
    "name: \(name)\n" +
    "items: \(items)\n"
  }
  
  
  override init() {}
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    name    <- map["category_name"]
    items   <- map["images"]
  }
  
  
}
