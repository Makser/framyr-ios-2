//
//  FRMShop.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMShop: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var shopID = 0
  var name = ""
  var address = ""
  var metro = [String]()
  var phone = [String]()
  var worktime = [String]()
  var longitude = 0.0
  var latitude = 0.0
  
  
  override var description: String {
    return "\n\nShop\n" +
    "id: \(shopID)\n" +
    "name: \(name)\n" +
    "address: \(address)\n" +
    "metro: \(metro)\n" +
    "phone: \(phone)\n" +
    "worktime: \(worktime)\n" +
    "long: \(longitude)\n" +
    "lat: \(latitude)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    shopID      <- (map["id"], intTransform)
    name        <- map["name"]
    address     <- map["address"]
    metro       <- map["metro"]
    phone       <- map["phone"]
    worktime    <- map["worktime"]
    longitude   <- (map["latitude"], doubleTransform)
    latitude    <- (map["longitude"], doubleTransform)
    
    
    address = address.html2String()
  }
  
  
}
