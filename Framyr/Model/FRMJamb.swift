//
//  FRMJamb.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMJamb: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var jambID = 0
  var name = ""
  var coveringPrice = [Int: Double]()
  var templateUrl = ""
  var url = ""
  
  
  override var description: String {
    return "\n\nJamb\n" +
    "id: \(jambID)\n" +
    "name: \(name)\n" +
    "cov. price: \(coveringPrice)\n" +
    "template: \(templateUrl)\n" +
    "url: \(url)\n"
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    jambID          <- (map["id"], intTransform)
    name            <- map["name"]
    coveringPrice   <- (map["covering_price"], intDoubleArrayTransform)
    templateUrl     <- map["template_url"]
    url             <- map["url"]
  }
  
  
}
