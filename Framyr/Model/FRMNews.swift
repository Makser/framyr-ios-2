//
//  FRMNews.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import Foundation

class FRMNews: NSObject, Mappable {

  
  
  //  MARK: -
  //  MARK: properties
  var newsID = 0
  var title = ""
  var descr = ""
  var url = ""
  var date = NSDate()
  
  
  var dateString: String {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "dd MMMM yyyy hh:mm"
    return formatter.stringFromDate(date) }
  
  
  var fullURL: NSURL? { return AppURL.Domain.url?.URLByAppendingPathComponent(url) }
  
  
  override var description: String {
    return "\n\nNews\n" +
    "id: \(newsID)\n" +
    "title: \(title)\n" +
    "descr: \(descr.shortString(10))...\n" +
    "url: \(url)\n" +
    "date: \(date)\n"
  
  }
  
  
  
  //  MARK: -
  //  MARK: Mappable
  required init?(_ map: Map) {
    
  }
  
  
  func mapping(map: Map) {
    newsID <- (map["id"], intTransform)
    title <- map["title"]
    descr <- map["description"]
    url <- map["url"]
    date <- (map["date"], dateTransform)
    
    title = title.html2String()
    descr = descr.html2String()
  }
  
  
}
