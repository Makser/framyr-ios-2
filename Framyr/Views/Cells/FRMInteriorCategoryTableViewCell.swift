//
//  FRMInteriorCategoryTableViewCell.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInteriorCategoryTableViewCell: FRMBaseTableViewCell {

  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var icoLabel: UILabel!
  
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    icoLabel.textColor = AppColors.accent.color
    icoLabel.font = icoLabel.font.fontWithSize(40)
  }
  
  
  func setData(category: FRMInteriorCategory) {
    titleLabel?.text = category.name
    icoLabel.attributedText = category.icon?.string
  }
  

}
