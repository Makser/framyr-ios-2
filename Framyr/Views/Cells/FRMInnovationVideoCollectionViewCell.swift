//
//  FRMInnovationVideoCollectionViewCell.swift
//  Framyr
//
//  Created by Admin on 27.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInnovationVideoCollectionViewCell: UICollectionViewCell {
 
  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var playIcon: UIImageView!
  
  
  
  //  MARK: -
  //  MARK: methods
  func setData(video: FRMMediaFile) {
    guard let url = video.fullURL?.lastPathComponent else { return }
    let thumbnail = "http://img.youtube.com/vi/" + "\(url)/mqdefault.jpg"
    
    self.playIcon.hidden = true
    imageView.setImageURL(NSURL(string: thumbnail)) { () -> Void in
      self.playIcon.hidden = false}

  }
  
  
}
