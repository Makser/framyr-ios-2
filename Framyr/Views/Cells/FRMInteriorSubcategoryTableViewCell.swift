//
//  FRMInteriorSubcategoryTableViewCell.swift
//  Framyr
//
//  Created by Admin on 21.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInteriorSubcategoryTableViewCell: FRMBaseTableViewCell {

  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var logoView: UIImageView?
  
  
  
  //  MARK: -
  //  MARK: methods
  func setData(category: FRMinterior) {
    titleLabel?.text = category.name
  }
  
  
  func setGlassData(glassCategory: FRMStainedGlassCategory) {
    titleLabel?.text = glassCategory.name
    logoView?.setImageURL(glassCategory.fullUrl)
  }
  
  
}
