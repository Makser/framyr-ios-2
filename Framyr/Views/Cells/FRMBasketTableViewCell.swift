//
//  FRMBasketTableViewCell.swift
//  Framyr
//
//  Created by Admin on 27.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


protocol FRMBasketTableViewCellDelegate {
  func minusTapped(cell: UITableViewCell)
  func plusTapped(cell: UITableViewCell)
}


class FRMBasketTableViewCell: FRMBaseTableViewCell {
  
  
  
  //  MARK: -
  //  MARK: properties
  var delegate: FRMBasketTableViewCellDelegate?
  
  
  @IBOutlet private weak var preview: UIImageView!
  @IBOutlet private weak var countLabel: UILabel!
  @IBOutlet private weak var priceLabel: UILabel!
  
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    titleLabel?.font = AppFonts.HelveticaNeueCyrRoman.font(12)
    priceLabel.font = AppFonts.HelveticaNeueCyrRoman.font(12)
    countLabel.font = AppFonts.HelveticaNeueCyrRoman.font(12)
    
    titleLabel?.textColor = AppColors.color1.color
    priceLabel.textColor = AppColors.color1.color
    countLabel.textColor = AppColors.color1.color
    
  }
  
  
  
  func setData(goods: FRMGoods) {
    titleLabel?.text = goods.name
    countLabel.text = "\(goods.count)"
    priceLabel.text = priceToString(goods.totalPrice)
    if let url = goods.fullURL { preview.setImageURL(url) }
    
  }
  
  
  override func setSelected(selected: Bool, animated: Bool) {
    
  }
  
  
  
  //  MARK: -
  //  MARK: actions
  @IBAction func minusTapped(sender: AnyObject) {
    delegate?.minusTapped(self)
  }
  
  
  @IBAction func plusTapped(sender: AnyObject) {
    delegate?.plusTapped(self)
  }
  
  
}


