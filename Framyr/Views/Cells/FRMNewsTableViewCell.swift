//
//  FRMNewsTableViewCell.swift
//  Framyr
//
//  Created by Admin on 22.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMNewsTableViewCell: FRMBaseTableViewCell {
  
  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var contentLabel: UILabel!
  @IBOutlet private weak var iconLabel: UILabel!
  @IBOutlet private weak var dateLabel: UILabel!
  
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    titleLabel?.font = AppFonts.HelveticaNeueCyrLight.font(17)
    contentLabel.font = AppFonts.HelveticaNeueCyrLight.font(12)
    dateLabel.font = AppFonts.HelveticaNeueCyrLight.font(12)
    
    iconLabel.fontSize = 40
    iconLabel.icon = Icomoon.Date
    
    removeGrid()
  }
  
  
  
  func setData(news: FRMNews) {
    titleLabel?.text = news.title
    contentLabel.text = news.descr
    dateLabel.text = news.dateString
  }
  
  
  override func setSelected(selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    if selected {
      contentLabel.textColor = AppColors.white.color
      dateLabel.textColor = AppColors.white.color
      iconLabel.textColor = AppColors.white.color
      
    } else {
      contentLabel.textColor = AppColors.color2.color
      dateLabel.textColor = AppColors.accent.color
      iconLabel.textColor = AppColors.accent.color
      
    }
    
  }
  
  
  func preferredHeight(preferredWidth: CGFloat) -> CGFloat {
    titleLabel?.preferredMaxLayoutWidth = preferredWidth
    contentLabel.preferredMaxLayoutWidth = preferredWidth
    
    setNeedsUpdateConstraints()
    updateConstraintsIfNeeded()
    
    bounds = CGRectMake(0, 0, preferredWidth, bounds.size.height)
    
    setNeedsLayout()
    layoutIfNeeded()
    
    return contentView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height
  }
  
  
}