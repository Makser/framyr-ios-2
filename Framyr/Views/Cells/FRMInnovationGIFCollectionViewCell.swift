//
//  FRMInnovationGIFCollectionViewCell.swift
//  Framyr
//
//  Created by Admin on 26.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


protocol FRMInnovationGIFCollectionViewCellDelegate {
  func didEndZoomingAtScale(scale: CGFloat)
}


class FRMInnovationGIFCollectionViewCell: UICollectionViewCell {
  
  
  
  //  MARK: -
  //  MARK: properties
  var delegate: FRMInnovationGIFCollectionViewCellDelegate?
  
  
  @IBOutlet private weak var imageView: FLAnimatedImageView!
  @IBOutlet private weak var scrollView: UIScrollView?
  
  
  private var gifUrl: NSURL?
  
  
  
  //  MARK: -
  //  MARK: methods
  func setData(media: FRMMediaFile) {
  
    guard let url = media.fullURL else { return }
    
    gifUrl = url

    imageView.animatedImage = nil
    MessageCenter.sharedInstance.showElementLoading(imageView)
    
    FRMAPILayer.sharedInstance.requestFile(url) { (gifData, responseUrl) -> () in
      if responseUrl == self.gifUrl {
        MessageCenter.sharedInstance.hideElementLoading(self.imageView)
        
        let image = FLAnimatedImage(GIFData: gifData)
        self.imageView.animatedImage = image
        
        self.scrollView?.minimumZoomScale = 1
        self.scrollView?.maximumZoomScale = 2
        self.scrollView?.contentSize = image.size
        self.scrollView?.delegate = self
        
        self.scrollView?.zoomScale = 1
      }
      
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UIScrollViewDelegate
extension FRMInnovationGIFCollectionViewCell : UIScrollViewDelegate {
  
  
  func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
    return imageView
  }
  
  
  func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
    delegate?.didEndZoomingAtScale(scale)
  }
  
  
}
