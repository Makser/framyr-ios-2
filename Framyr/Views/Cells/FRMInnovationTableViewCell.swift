//
//  FRMInnovationTableViewCell.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInnovationTableViewCell: FRMBaseTableViewCell {

  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var previewView: UIImageView!
  
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    titleLabel?.font = AppFonts.HelveticaNeueCyrRoman.font(17)
  }
  
  
  func setData(innovation: FRMDoorInnovation) {
    titleLabel?.text = innovation.name
    
    previewView.setImageURL(innovation.previewFullURL)
  }
  

}
