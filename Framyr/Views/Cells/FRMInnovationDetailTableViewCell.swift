//
//  TableViewCell.swift
//  Framyr
//
//  Created by Admin on 26.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMInnovationDetailTableViewCell: FRMBaseTableViewCell {
  
  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var containerView: UIView!
  
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    containerView.layer.borderWidth = 1
    containerView.layer.borderColor = UIColor.lightGrayColor().CGColor
  }
  
  
  func setData(title: String, dataView: UIView) {
    titleLabel?.text = title
    
    dataView.removeFromSuperview()
    containerView.addSubview(dataView)
    
    dataView.translatesAutoresizingMaskIntoConstraints = false
    
    let xConstraint = NSLayoutConstraint(item: dataView, attribute: .CenterX, relatedBy: .Equal, toItem: containerView, attribute: .CenterX, multiplier: 1, constant: 0)
    let yConstraint = NSLayoutConstraint(item: dataView, attribute: .CenterY, relatedBy: .Equal, toItem: containerView, attribute: .CenterY, multiplier: 1, constant: 0)
    let widthConstraint = NSLayoutConstraint(item: dataView, attribute: .Leading, relatedBy: .Equal, toItem: containerView, attribute: .Leading, multiplier: 1, constant: 0)
    let heightConstraint = NSLayoutConstraint(item: dataView, attribute: .Top, relatedBy: .Equal, toItem: containerView, attribute: .Top, multiplier: 1, constant: 0)
    
    containerView.addConstraints([xConstraint, yConstraint, widthConstraint, heightConstraint])
  
  }
  
  
}