//
//  FRMInteriorCollectionViewCell.swift
//  Framyr
//
//  Created by Admin on 21.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInteriorCollectionViewCell: UICollectionViewCell {
  
  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var priceLabel: UILabel!
  @IBOutlet private weak var view: UIView!
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    layer.borderWidth = 1
    layer.cornerRadius = 2
    layer.masksToBounds = false
    layer.borderColor = UIColor.lightGrayColor().CGColor
    layer.shadowRadius = 2
    layer.shadowOffset = CGSizeMake(2, 2)
    layer.shadowOpacity = 0.3
    
    titleLabel.font = AppFonts.HelveticaNeueCyrLight.font(14)
    titleLabel.textColor = AppColors.color0.color
    
    priceLabel.font = AppFonts.HelveticaNeueCyrRoman.font(14)
    priceLabel.textColor = AppColors.accent.color
    
    view.addGrid()
  }
  
  
  //  override func setSelected(selected: Bool, animated: Bool) {
  //    if selected {
  //      view.layer.shadowOpacity = 0
  //      view.backgroundColor = AppColors.accent.color
  //    }
  //    else {
  //      view.layer.shadowOpacity = 0.3
  //      view.backgroundColor = AppColors.color3.color
  //    }
  //
  //  }
  
  
  func setData(interior: FRMinterior) {
    titleLabel.text = interior.name
    priceLabel.text = priceToString(interior.price)
    imageView.setImageURL(interior.fullURL)
  }
  
  
  func setGlassData(stainedGlass: FRMStainedGlass) {
    titleLabel.text = stainedGlass.name
    priceLabel.text = priceToString(stainedGlass.price)
    imageView.setImageURL(stainedGlass.fullUrl)
  }
  
  
}
