//
//  FRMInteriorHeaderCollectionViewCell.swift
//  Framyr
//
//  Created by Admin on 21.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInteriorHeaderCollectionViewCell: UICollectionViewCell {
 
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var titleLabel: UILabel!
  
  
  
  //  MARK: -
  //  MARK: methods
  func setData(descr: String) {
    titleLabel.font = AppFonts.HelveticaNeueCyrLight.font(12)
    titleLabel.textColor = AppColors.color1.color
    
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.minimumLineHeight = 18
    
    let attributedString = NSAttributedString(string: descr, attributes: [
      NSParagraphStyleAttributeName : paragraphStyle
      ])
    
    titleLabel.attributedText = attributedString
  }
  
  
  func preferredHeight() -> CGFloat {
    return titleLabel.sizeThatFits(CGSize(width: frame.size.width - 40, height: CGFloat.max)).height

  }
  
  
}
