//
//  FRMLeftSideTableViewCell.swift
//  Framyr
//
//  Created by Admin on 14.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMLeftSideTableViewCell: UITableViewCell {

  
  enum CellType {
    case Header
    case Content
  }
  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var separatorView: UIView!
  @IBOutlet private weak var view: UIView!
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    separatorView.backgroundColor = AppColors.accent.color
    selectionStyle = .None
    
    view.addGrid()
    view.backgroundColor = AppColors.accent.color
   
  }
  
  
  override func setSelected(selected: Bool, animated: Bool) {
    view.hidden = !selected
    
  }
  
  
  func setData(title: String, type: CellType, isLast: Bool) {
    titleLabel.text = title
    separatorView.hidden = isLast
    
    switch type {
    case .Header:
      titleLabel.font = AppFonts.HelveticaNeueCyrLight.font(14)
      titleLabel.textColor = AppColors.accent.color
      userInteractionEnabled = false
    case .Content:
      titleLabel.font = AppFonts.HelveticaNeueCyrLight.font(12)
      titleLabel.textColor = AppColors.white.color
    }
    
  }
  

}
