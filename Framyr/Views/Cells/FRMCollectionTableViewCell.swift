//
//  FRMCollectionTableViewCell.swift
//  Framyr
//
//  Created by Admin on 15.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMCollectionTableViewCell: UITableViewCell {

  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var backgroundImageView: UIImageView?
  @IBOutlet private weak var titleImageView: UIImageView?
  @IBOutlet private weak var titleLabel: UILabel?
  @IBOutlet private weak var view: UIView!
  @IBOutlet private weak var accessoryIcon: UILabel?
  
  
  
  //  MARK: -
  //  MARK: methods
  override func awakeFromNib() {
    super.awakeFromNib()
    
    selectionStyle = .None
    
    view.layer.borderWidth = 1
    view.layer.cornerRadius = 2
    view.layer.masksToBounds = false
    view.layer.borderColor = UIColor.lightGrayColor().CGColor
    view.layer.shadowRadius = 2
    view.layer.shadowOffset = CGSizeMake(2, 2)
    view.layer.shadowOpacity = 0.3
    
    accessoryIcon?.icon = Icomoon.ArrowRight
    accessoryIcon?.fontSize = 30
    accessoryIcon?.textColor = AppColors.accent.color
    
    titleLabel?.font = AppFonts.HelveticaNeueCyrRoman.font(17)
    titleLabel?.textColor = AppColors.color1.color
    
    if let _ = titleLabel { view.addGrid() }
  }
  
  
  override func setSelected(selected: Bool, animated: Bool) {
    if selected {
      view.layer.shadowOpacity = 0
      view.backgroundColor = AppColors.accent.color
    }
    else {
      view.layer.shadowOpacity = 0.3
      view.backgroundColor = AppColors.white.color
    }
    
  }
  
  
  func setData(collection: FRMDoorCollection) {
    backgroundImageView?.image = collection.image
    titleImageView?.image = collection.titleImage
    titleLabel?.text = collection.name
  }
  
  
}
