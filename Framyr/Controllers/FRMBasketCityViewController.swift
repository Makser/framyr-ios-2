//
//  FRMBasketCityViewController.swift
//  Framyr
//
//  Created by Admin on 28.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMBasketCityViewController: UIViewController {

  
  
  //  MARK: -
  //  MARK: properties
  var completion: (()->())?
  
  
  private let cellHeight: CGFloat = 40
  
  
  private var items = [String]()
  private var selectedCity: String = "" {
    didSet { updateCityButton() }
  }
  
  
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var closeButton: UIButton!
  @IBOutlet private weak var cityButton: UIButton!
  @IBOutlet private weak var resetButton: UIButton!
  @IBOutlet private weak var confirmButton: UIButton!
  @IBOutlet private weak var iconLabel: UILabel!
  @IBOutlet private weak var tableHeightConstraint: NSLayoutConstraint!
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMBasketCityViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureInterface()
    
    loadData()
  }
  

}



//  MARK: -
//  MARK: data
extension FRMBasketCityViewController {
  
  
  func loadData() {
    selectedCity = Singletone.sharedInstance.userCityName
    items = Array(Set<String>(Singletone.sharedInstance.shops.reduce([String](), combine: { $0 + [$1.name] })))
      .sort( { $0.compare($1) == NSComparisonResult.OrderedAscending } )
    
    
    titleLabel.text = NSLocalizedString("Выберите город:", comment: "")
    closeButton.icon = Icomoon.Close
    resetButton.setTitle(NSLocalizedString("Сбросить", comment: ""), forState: .Normal)
    confirmButton.setTitle(NSLocalizedString("Применить", comment: ""), forState: .Normal)
    
    iconLabel.icon = Icomoon.Select
    
    updateCityButton()
  }
  
  
  func updateCityButton() {
    cityButton.setTitle(selectedCity, forState: .Normal)
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMBasketCityViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMBasketCityViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    let item = items[indexPath.row]
    
    cell.textLabel?.text = item
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let item = items[indexPath.row]
    selectedCity = item
    
    hideTable()
    
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: interface
extension FRMBasketCityViewController {
  
  
  func configureInterface() {
    titleLabel.font = AppFonts.HelveticaNeueCyrLight.font(17)
    
    titleLabel.textColor = AppColors.color1.color
    closeButton.setTitleColor(AppColors.color1.color, forState: .Normal)
    cityButton.setTitleColor(AppColors.color1.color, forState: .Normal)
    
    cityButton.addGrid()
  }
  
  
  func showTable() {
    tableHeightConstraint.constant = CGFloat(items.count) * cellHeight

    self.view.setNeedsUpdateConstraints()
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view.layoutIfNeeded()
    }
    
  }
  
  
  func hideTable() {
    tableHeightConstraint.constant = 0
    
    self.view.setNeedsUpdateConstraints()
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view.layoutIfNeeded()
    }
    
  }
  
  
}



//  MARK: -
//  MARK: actions
extension FRMBasketCityViewController {
  
  
  @IBAction func resetTapped(sender: AnyObject) {
    selectedCity = Singletone.sharedInstance.userCityName
  
  }
  
  
  @IBAction func confirmTapped(sender: AnyObject) {
    Singletone.sharedInstance.userCityName = selectedCity
    completion?()
    
  }
  
  
  @IBAction func closeButton(sender: AnyObject) {
    completion?()
    
  }
  
  
  @IBAction func cityTapped(sender: AnyObject) {
    if tableHeightConstraint.constant == 0 { showTable() }
    else { hideTable() }
    
  }
  
  
}