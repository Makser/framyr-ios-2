//
//  FRMLeftSideMenuViewController.swift
//  Framyr
//
//  Created by Сергей Максимук on 10.11.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMLeftSideMenuViewController: FRMBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  private var items: [ (String, FRMLeftSideTableViewCell.CellType, Segue?) ] = [
    (NSLocalizedString("КАТАЛОГ", comment: ""), .Header, nil),
//    (NSLocalizedString("Двери Фрамир", comment: ""), .Content, FRMLeftSideMenuViewController.Segue.segueMenuToCollections),
//    (NSLocalizedString("Двери Fineza Puerta", comment: ""), .Content, nil),
    (NSLocalizedString("Инновационные дверные системы", comment: ""), .Content, FRMLeftSideMenuViewController.Segue.segueMenuToInnovations),
    (NSLocalizedString("Интерьерная продукция", comment: ""), .Content, FRMLeftSideMenuViewController.Segue.segueMenuToInterior),
    (NSLocalizedString("О НАС", comment: ""), .Header, nil),
    (NSLocalizedString("Где купить", comment: ""), .Content, FRMLeftSideMenuViewController.Segue.segueMenuToMap),
    (NSLocalizedString("Обратная связь", comment: ""), .Content, FRMLeftSideMenuViewController.Segue.segueMenuToContact),
    (NSLocalizedString("Новости", comment: ""), .Content, FRMLeftSideMenuViewController.Segue.segueMenuToNews),
  ]
  
  
  @IBOutlet private weak var tableView: UITableView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private let cellHeight: CGFloat = 45
  

}



//  MARK: -
//  MARK: VLC
extension FRMLeftSideMenuViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    let inset = (view.bounds.size.height - cellHeight * CGFloat(items.count)) / 2
    if inset > 0 {
      tableView.contentInset = UIEdgeInsetsMake(inset, 0, -inset, 0)
    }
    
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMLeftSideMenuViewController {
  
  
  func loadData() {
    var index = 1
    for catalog in Singletone.sharedInstance.doorCatalogs {
      items.insert((catalog.name, .Content, FRMLeftSideMenuViewController.Segue.segueMenuToCollections), atIndex: index++)
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMLeftSideMenuViewController: UITableViewDataSource {

  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(FRMLeftSideMenuViewController.Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMLeftSideMenuViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMLeftSideTableViewCell {
      let item = items[indexPath.row]
      cell.setData(item.0, type: item.1, isLast: indexPath.row == (items.count - 1))

      if item.1 == FRMLeftSideTableViewCell.CellType.Content && item.2 == nil {
        cell.alpha = 0.3
      }
      
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    let item = items[indexPath.row]
    if let segue = item.2 { performSegue(segue, sender: indexPath) }
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMLeftSideMenuViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueMenuToCollections {
      let catalog = Singletone.sharedInstance.doorCatalogs[indexPath.row - 1]
      let destinitionController = (segue.destinationViewController as! UINavigationController).topViewController as! FRMCollectionListViewController
      
      destinitionController.catalog = catalog
    }
    
  }
  
  
}