//
//  FRMCollectionListViewController.swift
//  Framyr
//
//  Created by Admin on 15.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMCollectionListViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  var catalog: FRMDoorCatalog?
  
  
  private var items: [ FRMDoorCollection ] = []
  
  
  @IBOutlet private weak var tableView: UITableView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private var cellHeight: CGFloat = 50 {
    didSet { tableView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMCollectionListViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    cellHeight = tableView.bounds.size.height / items.count
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMCollectionListViewController {
  
  
  func loadData() {
    if let catalog = catalog {
      titleLabel.text = catalog.name
      
      items = catalog.collections
    }

  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMCollectionListViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let item = items[indexPath.row]
    if item.image != nil && item.titleImage != nil {
      let cell = tableView.dequeueReusableCell(FRMCollectionListViewController.Reusable.cellOld, forIndexPath: indexPath)
      return cell!
    }
    
    let cell = tableView.dequeueReusableCell(FRMCollectionListViewController.Reusable.cellNew, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMCollectionListViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMCollectionTableViewCell {
      let item = items[indexPath.row]
      cell.setData(item)
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    performSegue(Segue.segueCollectionToDoors, sender: indexPath)
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMCollectionListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueCollectionToDoors {
      let destinitionController = segue.destinationViewController as! FRMDoorListViewController
      let collection = items[indexPath.row]
      
      destinitionController.collection = collection
    }
    
  }
  
  
}