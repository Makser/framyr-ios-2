//
//  FRMInteriorDetailViewController.swift
//  Framyr
//
//  Created by Admin on 27.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMInteriorDetailViewController: FRMBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  var interior: FRMinterior?
  var stainedGlass: FRMStainedGlass?
  
  
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var nameLabel: UILabel!
  @IBOutlet private weak var priceLabel: UILabel!
  @IBOutlet private weak var buyButton: UIButton!
  @IBOutlet private weak var bottomView: UIView!
  

}



//  MARK: -
//  MARK: VLC
extension FRMInteriorDetailViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureInterface()
    
    loadData()
    
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMInteriorDetailViewController {
  
  
  func loadData() {
    if let interior = interior {
      titleLabel.text = interior.name
    
      nameLabel.text = interior.name
      priceLabel.text = priceToString(interior.price)
      imageView.setImageURL(interior.fullURL) }
    else if let stainedGlass = stainedGlass {
      titleLabel.text = stainedGlass.name
      
      nameLabel.text = stainedGlass.name
      priceLabel.text = priceToString(stainedGlass.price)
      imageView.setImageURL(stainedGlass.fullUrl) }
    
    buyButton.setTitle(NSLocalizedString("Купить", comment: ""), forState: .Normal)
  }
  
  
}



//  MARK: -
//  MARK: actions
extension FRMInteriorDetailViewController {
  
  
  @IBAction func buyTapped(sender: AnyObject) {
    if let interior = interior {
      Singletone.sharedInstance.basket.addInterior(interior) }
    else if let glass = stainedGlass {
      Singletone.sharedInstance.basket.addGlass(glass) }
    
    performSegue(Segue.segueDetailToBasket)
  }
  
  
}



//  MARK: -
//  MARK: interface
extension FRMInteriorDetailViewController {
  
  
  func configureInterface() {
    bottomView.addGrid()
    
    nameLabel.font = AppFonts.HelveticaNeueCyrLight.font(17)
    priceLabel.font = AppFonts.HelveticaNeueCyrRoman.font(17)
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMInteriorDetailViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue == Segue.segueDetailToZoom {
      let destinition = segue.destinationViewController as! FRMZoomingViewController
      
      if let interior = interior { destinition.imageUrl = interior.fullURL }
      if let stainedGlass = stainedGlass { destinition.imageUrl = stainedGlass.fullUrl }
    }
    
  }
  
  
}
