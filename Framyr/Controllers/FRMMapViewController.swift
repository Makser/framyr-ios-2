//
//  FRMMapViewController.swift
//  Framyr
//
//  Created by Admin on 28.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMMapViewController: UIViewController {


  
  //  MARK: -
  //  MARK: properties
  private let cellHeight: CGFloat = 40
  private let kShopTableTag = 1
  private let kCityTableTag = 2
  
  
  private var items = [FRMShop]() {
    didSet { tableView.reloadData() }
  }
  private var selectedCity: String = "" {
    didSet { updateCity() }
  }
  private var dropItems = [String]()
  
  
  
  @IBOutlet private weak var mapView: UIView!
  @IBOutlet private weak var cityButton: UIButton!
  @IBOutlet private weak var filterButton: UIButton!
  @IBOutlet private weak var tableView: UITableView!
  @IBOutlet private weak var topView: UIView!
  @IBOutlet private weak var iconLabel: UILabel!
  @IBOutlet private weak var dropTableView: UITableView!
  @IBOutlet private weak var dropTableHeight: NSLayoutConstraint!

  
}



//  MARK: -
//  MARK: VLC
extension FRMMapViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureInterface()
    
    loadData()
  }

  
}



//  MARK: -
//  MARK: data
extension FRMMapViewController {
  
  
  func loadData() {
    selectedCity = Singletone.sharedInstance.userCityName
    
    if let city = Singletone.sharedInstance.userCity { items = city.shops }
    dropItems = Array(Set<String>(Singletone.sharedInstance.shops.reduce([String](), combine: { $0 + [$1.name] })))
    .sort( { $0.compare($1) == NSComparisonResult.OrderedAscending } )
    
    iconLabel.icon = Icomoon.Select
    filterButton.icon = Icomoon.Filter
    
    updateCity()
    
    
    tableView.tag = kShopTableTag
    dropTableView.tag = kCityTableTag
  }
  
  
  func updateCity() {
    cityButton.setTitle(selectedCity, forState: .Normal)
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMMapViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch tableView.tag {
    case kCityTableTag: return dropItems.count
    case kShopTableTag: return items.count
    default : return 0
    }
    
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch tableView.tag {
    case kShopTableTag:
      let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
      
      cell?.textLabel?.font = AppFonts.HelveticaNeueCyrLight.font(12)
      cell?.textLabel?.numberOfLines = 0
      cell?.textLabel?.textColor = AppColors.color3.color
      cell?.textLabel?.textAlignment = .Center
      
      return cell!
    case kCityTableTag:
      let cell = tableView.dequeueReusableCell(Reusable.menuCell, forIndexPath: indexPath)
      
      return cell!
    default: return UITableViewCell()
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMMapViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    switch tableView.tag {
    case kShopTableTag:
      let item = items[indexPath.row]
      
      var text = item.address
      if let phone = item.phone.first { text = text + ", тел.: " + phone }
      
      cell.textLabel?.text = text
    case kCityTableTag:
      let item = dropItems[indexPath.row]
      
      cell.textLabel?.text = item
    default: break
    }
    
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    switch tableView.tag {
    case kCityTableTag:
      let item = dropItems[indexPath.row]
      selectedCity = item
      
      hideTable()
    default: break
    }
    
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: actions
extension FRMMapViewController {
  
  
  @IBAction func cityTapped(sender: AnyObject) {
    if dropTableHeight.constant == 0 { showTable() }
    else { hideTable() }
    
  }
  
  
}



//  MARK: -
//  MARK: interface
extension FRMMapViewController {
  
  
  func configureInterface() {
    topView.addGrid()
    
    filterButton.titleLabel?.fontSize = 30
    
    cityButton.setTitleColor(AppColors.color1.color, forState: .Normal)
    filterButton.setTitleColor(AppColors.color1.color, forState: .Normal)
  }
  
  
  func showTable() {
    dropTableHeight.constant = CGFloat(dropItems.count) * cellHeight
    
    self.view.setNeedsUpdateConstraints()
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view.layoutIfNeeded()
    }
    
  }
  
  
  func hideTable() {
    dropTableHeight.constant = 0
    
    self.view.setNeedsUpdateConstraints()
    UIView.animateWithDuration(0.7) { () -> Void in
      self.view.layoutIfNeeded()
    }
    
  }
  
  
}