//
//  FRMDoorListViewController.swift
//  Framyr
//
//  Created by Admin on 15.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMDoorListViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  var collection: FRMDoorCollection?
  
  
  private var items: [ FRMDoor ] = []
  
  
  @IBOutlet private weak var collectionView: UICollectionView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private let cellInset: CGFloat = 4
  private var cellSize: CGSize = CGSizeZero {
    didSet { collectionView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMDoorListViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let width = collectionView.bounds.size.width / 2 - 2 * cellInset
    let height = 3 / 2 * width
    cellSize = CGSizeMake(width, height)
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMDoorListViewController {
  
  
  func loadData() {
    if let collection = collection {
      titleLabel.text = collection.name
      items = collection.doors
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDataSource
extension FRMDoorListViewController: UICollectionViewDataSource {
  
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(FRMDoorListViewController.Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDelegate
extension FRMDoorListViewController: UICollectionViewDelegate {
  
  
  func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMDoorCollectionViewCell {
      let item = items[indexPath.row]
      cell.setData(item)
    }
    
  }

  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
  
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDelegateFlowLayout
extension FRMDoorListViewController: UICollectionViewDelegateFlowLayout {
  

  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return cellSize
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    return cellInset
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return cellInset * 2
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsMake(cellInset, cellInset, cellInset, cellInset)
  }
  
  
}