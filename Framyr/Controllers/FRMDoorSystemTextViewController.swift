//
//  FRMDoorSystemTextViewController.swift
//  Framyr
//
//  Created by Admin on 26.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMDoorSystemTextViewController: UIViewController {

  
  
  //  MARK: -
  //  MARK: properties
  var innovation: FRMDoorInnovation?
  
  
  @IBOutlet private weak var textView: UITextView!

  
  
  //  MARK: -
  //  MARK: methods
  override func viewDidLoad() {
    super.viewDidLoad()
  
    guard let innovation = innovation else { return }
    
    let style = NSMutableParagraphStyle()
    style.minimumLineHeight = 16
    
    let text = NSAttributedString(string: innovation.descr, attributes: [
      NSFontAttributeName : AppFonts.HelveticaNeueCyrLight.font(12)!,
      NSForegroundColorAttributeName : AppColors.color3.color,
      NSParagraphStyleAttributeName : style
      ])
    
    textView.attributedText = text
    
  }
  
  
}
