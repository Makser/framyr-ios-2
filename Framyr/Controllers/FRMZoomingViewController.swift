//
//  FRMZoomingViewController.swift
//  Framyr
//
//  Created by Admin on 27.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMZoomingViewController: FRMBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  var imageUrl: NSURL?
  var image: UIImage?
  
  
  @IBOutlet private weak var imageView: UIImageView!
  @IBOutlet private weak var scrollView: UIScrollView!

  
}



//  MARK: -
//  MARK: VLC
extension FRMZoomingViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMZoomingViewController {
  
  
  func loadData() {
    titleLabel.text = NSLocalizedString("Галерея", comment: "")
    
    if let imageUrl = imageUrl {
      imageView.setImageURL(imageUrl) { () -> Void in
        self.scrollView.minimumZoomScale = 1
        self.scrollView.maximumZoomScale = 2
        self.scrollView.contentSize = self.imageView.image?.size ?? self.view.bounds.size
        self.scrollView.zoomScale = 1}
    } else {
    
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UIScrollViewDelegate
extension FRMZoomingViewController : UIScrollViewDelegate {

  
  func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
    return imageView
  }
  
  
}