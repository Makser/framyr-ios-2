//
//  FRMInteriorCategoryListViewController.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit



class FRMInteriorCategoryListViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  private var items: [ FRMInteriorCategory ] = []
  
  
  @IBOutlet private weak var tableView: UITableView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private var cellHeight: CGFloat = 100 {
    didSet { tableView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMInteriorCategoryListViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    if let indexPath = tableView.indexPathForSelectedRow {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMInteriorCategoryListViewController {
  
  
  func loadData() {
    titleLabel.text = NSLocalizedString("Интерьерная продукция", comment: "")
    items = Singletone.sharedInstance.interior
    
    //
    let stainedGlasses = FRMInteriorCategory()
    stainedGlasses.name = "Витражи"
    items += [stainedGlasses]
    
    items = items.sort({ (item0, item1) -> Bool in
      return item0.name.compare(item1.name) == NSComparisonResult.OrderedAscending
    })
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMInteriorCategoryListViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(FRMInteriorCategoryListViewController.Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMInteriorCategoryListViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMInteriorCategoryTableViewCell {
      let item = items[indexPath.row]
      cell.setData(item)
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    performSegue(Segue.segueCategoryToSubcategory, sender: indexPath)
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMInteriorCategoryListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueCategoryToSubcategory {
      let destinitionController = segue.destinationViewController as! FRMInteriorSubcategoryViewController
      let item = items[indexPath.row]
      
      if item.name != "Витражи" {
        let category = items[indexPath.row]
        destinitionController.category = category }
      else {
        let stainedGlasses = Singletone.sharedInstance.stainedGlasses
        destinitionController.stainedGlasses = stainedGlasses }
      
    }

  }
  
  
}