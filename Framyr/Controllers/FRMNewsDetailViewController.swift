//
//  FRMNewsDetailViewController.swift
//  Framyr
//
//  Created by Admin on 22.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMNewsDetailViewController: FRMBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  var news: FRMNews?
  
  
  private let banList = [
    "share.yandex.net",
    "odnoklassniki.ru",
    "vk.com",
    "twitter.com"
  ]
  
  
  @IBOutlet private weak var webView: UIWebView!
  

}



//  MARK: -
//  MARK: VLC
extension FRMNewsDetailViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
  
    loadData()
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMNewsDetailViewController {
  
  
  func loadData() {
    titleLabel.text = NSLocalizedString("Новости", comment: "")
    
    guard let url = news?.fullURL else { return }
    
    let request = NSURLRequest(URL: url)
    webView.loadRequest(request)
  }
  
  
  func isUrlBanned(url: String) -> Bool {
    for link in banList {
      if url.rangeOfString(link) != nil { return true }
    }
    
    return false
  }
  
  
}



//  MARK: -
//  MARK: UIWebViewDelegate
extension FRMNewsDetailViewController: UIWebViewDelegate {
  
  
  func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
    guard let externalUrl = request.URL else { return false }
    let externalPath = externalUrl.absoluteString
    
    if isUrlBanned(externalPath) {
      UIApplication.sharedApplication().openURL(externalUrl)
      return false }
    
    return true
  }
  
  
  func webViewDidStartLoad(webView: UIWebView) {
    MessageCenter.sharedInstance.showElementLoading(webView)
  }
  
  
  func webViewDidFinishLoad(webView: UIWebView) {
    MessageCenter.sharedInstance.hideElementLoading(webView)
  }
  
  
}

