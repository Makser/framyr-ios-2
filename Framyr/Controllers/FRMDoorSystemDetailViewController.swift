//
//  FRMInnovationDetailViewController.swift
//  Framyr
//
//  Created by Admin on 26.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMDoorSystemDetailViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  var innovation: FRMDoorInnovation?
  
  
  private var items = [
    NSLocalizedString("Описание", comment: ""),
    NSLocalizedString("Механизм", comment: ""),
    NSLocalizedString("Видео", comment: "")
  ]
  
  
  private var textVC: FRMDoorSystemTextViewController!
  private var gifVC: FRMDoorSystemsGIFViewController!
  private var videoVC: FRMDoorSystemVideoListViewController!
  
  
  private var selectedIndexPath: NSIndexPath! = NSIndexPath(forItem: 0, inSection: 0) {
    didSet {
      viewForIndex(oldValue.row).hidden = true
      viewForIndex(selectedIndexPath.row).hidden = false
      
      tableView.reloadData()
      tableView.selectRowAtIndexPath(selectedIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.None)
    }
  }
  
  
  @IBOutlet private weak var tableView: UITableView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private var cellHeight: CGFloat = 84 {
    didSet { tableView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMDoorSystemDetailViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    textVC = Storyboards.Main.instantiateDoorSystemText()
    self.addChildViewController(textVC)
    textVC.didMoveToParentViewController(self)
    textVC.innovation = innovation
    textVC.view.hidden = true
    
    gifVC = Storyboards.Main.instantiateDoorSystemGIF()
    self.addChildViewController(gifVC)
    gifVC.didMoveToParentViewController(self)
    gifVC.innovation = innovation
    gifVC.view.hidden = true
    gifVC.delegate = self
    
    videoVC = Storyboards.Main.instantiateDoorSystemVideo()
    self.addChildViewController(videoVC)
    videoVC.didMoveToParentViewController(self)
    videoVC.innovation = innovation
    videoVC.view.hidden = true
    
    loadData()
    
    selectedIndexPath = NSIndexPath(forRow: 0, inSection: 0)
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMDoorSystemDetailViewController {
  
  
  func loadData() {
    guard let innovation = innovation else { return }
    titleLabel.text = innovation.name
    
  }
  
  
  func viewForIndex(index: Int) -> UIView {
    switch index {
    case 0: return textVC.view
    case 1: return gifVC.view
    case 2: return videoVC.view
    default: return UIView()
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMDoorSystemDetailViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMDoorSystemDetailViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMInnovationDetailTableViewCell {
      let item = items[indexPath.row]
      let dataView = viewForIndex(indexPath.row)
      
      cell.setData(item, dataView: dataView)
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    selectedIndexPath = indexPath
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath == selectedIndexPath {
      return tableView.bounds.size.height - 2 * cellHeight - 4
    }
    
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: FRMDoorSystemsGIFViewControllerDelegate
extension FRMDoorSystemDetailViewController : FRMDoorSystemsGIFViewControllerDelegate {
  
  
  func didSelectIndexPath(indexPath: NSIndexPath) {
    performSegue(Segue.segueDetailToGifList, sender: indexPath)
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMDoorSystemDetailViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueDetailToGifList {
      let destinitionController = segue.destinationViewController as! FRMDoorSystemsGIFViewController

      destinitionController.innovation = innovation
      destinitionController.startIndexPath = indexPath
    }
    
  }
  
  
}