//
//  FRMInteriorSubcategoryViewController.swift
//  Framyr
//
//  Created by Admin on 21.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMInteriorSubcategoryViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  var category: FRMInteriorCategory?
  var stainedGlasses: [FRMStainedGlassCategory]?
  
  
  private var items: [ AnyObject ] = []
  
  
  @IBOutlet private weak var tableView: UITableView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private var cellHeight: CGFloat = 100 {
    didSet { tableView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMInteriorSubcategoryViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    if let indexPath = tableView.indexPathForSelectedRow {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMInteriorSubcategoryViewController {
  
  
  func loadData() {
    if let category = category {
      titleLabel.text = category.name
      items = category.items }
    else if let stainedGlasses = stainedGlasses {
      titleLabel.text = "Витражи"
      items = stainedGlasses }
    
    items = items.sort({ (item0, item1) -> Bool in
      return item0.name.compare(item1.name) == NSComparisonResult.OrderedAscending
    })
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMInteriorSubcategoryViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: UITableViewCell!
    if let _ = category {
      cell = tableView.dequeueReusableCell(FRMInteriorSubcategoryViewController.Reusable.cell, forIndexPath: indexPath) }
    else {
      cell = tableView.dequeueReusableCell(Reusable.cellGlass, forIndexPath: indexPath) }
    return cell
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMInteriorSubcategoryViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMInteriorSubcategoryTableViewCell {
      let item = items[indexPath.row]
      
      switch item {
      case let category as FRMinterior: cell.setData(category)
      case let glassCategory as FRMStainedGlassCategory: cell.setGlassData(glassCategory)
      default: break
      }
      
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    performSegue(Segue.segueSubcategoryToList, sender: indexPath)
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMInteriorSubcategoryViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueSubcategoryToList {
      let destinitionController = segue.destinationViewController as! FRMInteriorListViewController
      
      if let category = category {
        destinitionController.interior = category.items.first }
      else if let stainedGlasses = stainedGlasses {
        let stainedGlass = stainedGlasses[indexPath.row]
        destinitionController.stainedGlass = stainedGlass }
      
    }
    
  }
  
  
}