//
//  FRMDoorSystemsGIFViewController.swift
//  Framyr
//
//  Created by Admin on 26.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


protocol FRMDoorSystemsGIFViewControllerDelegate {
  func didSelectIndexPath(indexPath: NSIndexPath)
}



class FRMDoorSystemsGIFViewController: FRMBaseViewController {
  
  
  private enum Mode {
    case In //  встроенный
    case Full //  полный экран
  }
  
  
  
  //  MARK: -
  //  MARK: properties
  var innovation: FRMDoorInnovation?
  var delegate: FRMDoorSystemsGIFViewControllerDelegate?
  var startIndexPath: NSIndexPath?
  
  
  private var items: [ FRMMediaFile ] = []
  private var pageCount: Int {
    switch mode {
    case .In: return 2
    case .Full: return 1
    }
  }
  private var mode: Mode { return delegate != nil ? Mode.In : Mode.Full }
  
  
  @IBOutlet private weak var collectionView: UICollectionView!;
  @IBOutlet private weak var prevButton: UIButton!
  @IBOutlet private weak var nextButton: UIButton!
  
  
  
  //  MARK: -
  //  MARK: constants
  private let cellInset: CGFloat = 4
  private var cellSize: CGSize = CGSizeZero {
    didSet { collectionView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMDoorSystemsGIFViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureInterface()
    
    loadData()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    configureButtons()
    
    let width = collectionView.bounds.size.width - 2 * cellInset
    let height = collectionView.bounds.size.height / pageCount - 2 * cellInset
    cellSize = CGSizeMake(max(0, width), max(0, height))
    
    if let indexPath = startIndexPath {
      collectionView.scrollToItemAtIndexPath(indexPath, atScrollPosition: .None, animated: false)
      startIndexPath = nil
    }
    
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMDoorSystemsGIFViewController {
  
  
  func loadData() {
    if mode == .Full {
      titleLabel.text = innovation?.name
    }
    
    if let innovation = innovation {
      items = innovation.gif
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDataSource
extension FRMDoorSystemsGIFViewController: UICollectionViewDataSource {
  
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 1
  }
  
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell: UICollectionViewCell!
    switch mode {
    case .In: cell = collectionView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
    case .Full: cell = collectionView.dequeueReusableCell(Reusable.cellZoom, forIndexPath: indexPath)
    }
    
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDelegate
extension FRMDoorSystemsGIFViewController: UICollectionViewDelegate {
  
  
  func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMInnovationGIFCollectionViewCell {
      let item = items[indexPath.row]
      
      cell.delegate = self
      cell.setData(item)
      
    }
    
  }
  
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    delegate?.didSelectIndexPath(indexPath)
  }
  
  
  func scrollViewDidScroll(scrollView: UIScrollView) {
    configureButtons()

  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDelegateFlowLayout
extension FRMDoorSystemsGIFViewController: UICollectionViewDelegateFlowLayout {
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return cellSize
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    return cellInset
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return cellInset * 2
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsMake(cellInset, cellInset, cellInset, cellInset)
  }
  
  
}



//  MARK: -
//  MARK: FRMInnovationGIFCollectionViewCellDelegate
extension FRMDoorSystemsGIFViewController: FRMInnovationGIFCollectionViewCellDelegate {
  
  
  func didEndZoomingAtScale(scale: CGFloat) {
    let val = scale > 1
    prevButton.hidden = val
    nextButton.hidden = val
  }
  
  
}



//  MARK: -
//  MARK: interface
extension FRMDoorSystemsGIFViewController {
  
  
  func configureInterface() {
    prevButton.icon = Icomoon.ArrowLeft
    nextButton.icon = Icomoon.ArrowRight
    
    prevButton.titleLabel?.fontSize = 30
    nextButton.titleLabel?.fontSize = 30
    
    prevButton.setTitleColor(AppColors.accent.color, forState: .Normal)
    nextButton.setTitleColor(AppColors.accent.color, forState: .Normal)
  }
  
  
  func configureButtons() {
    collectionView.collectionViewLayout.invalidateLayout()
    
    prevButton.hidden = collectionView.contentOffset.x < 50
    nextButton.hidden = collectionView.contentOffset.x + collectionView.bounds.size.width > collectionView.collectionViewLayout.collectionViewContentSize().width - 50

  }
  
  
}



//  MARK: -
//  MARK: actions
extension FRMDoorSystemsGIFViewController {
  
  
  @IBAction func prevTapped(sender: AnyObject) {
    guard let last = collectionView.indexPathsForVisibleItems().last else { return }
    guard let first = collectionView.indexPathsForVisibleItems().first else { return }
    
    let index = min(last.item, first.item)
    
    let prev = NSIndexPath(forItem: max(0, index - pageCount), inSection: 0)
    collectionView.scrollToItemAtIndexPath(prev, atScrollPosition: .None, animated: true)
  }
  
  
  @IBAction func nextTapped(sender: AnyObject) {
    guard let last = collectionView.indexPathsForVisibleItems().last else { return }
    guard let first = collectionView.indexPathsForVisibleItems().first else { return }
    
    let index = max(last.item, first.item)
    
    let next = NSIndexPath(forItem: min(items.count - 1, index + 1), inSection: 0)
    collectionView.scrollToItemAtIndexPath(next, atScrollPosition: .None, animated: true)
  }
  
  
}