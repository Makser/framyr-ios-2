//
//  FRMNewsListViewController.swift
//  Framyr
//
//  Created by Admin on 22.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMNewsListViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  private var items: [ FRMNews ] = []
  
  
  private var prototypeCell: FRMNewsTableViewCell!
  
  
  @IBOutlet private weak var tableView: UITableView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private var cellHeight: CGFloat = 200 {
    didSet { tableView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMNewsListViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    prototypeCell = tableView.dequeueReusableCellWithIdentifier(Reusable.cell.rawValue) as! FRMNewsTableViewCell
    
    loadData()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    if let indexPath = tableView.indexPathForSelectedRow {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMNewsListViewController {
  
  
  func loadData() {
    items = Singletone.sharedInstance.news
    titleLabel.text = NSLocalizedString("Новости", comment: "")
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMNewsListViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMNewsListViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMNewsTableViewCell {
      let item = items[indexPath.row]
      cell.setData(item)
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    performSegue(Segue.segueListToDetail, sender: indexPath)
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    let item = items[indexPath.row]
    
    prototypeCell.setData(item)
    
    let height = prototypeCell.preferredHeight(300)
    
    return height
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMNewsListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueListToDetail {
      let destinitionController = segue.destinationViewController as! FRMNewsDetailViewController
      let news = items[indexPath.row]
      
      destinitionController.news = news
    }
    
  }
  
  
}