//
//  FRMDoorSystemsListViewController.swift
//  Framyr
//
//  Created by Admin on 16.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMDoorSystemsListViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  private var items: [ FRMDoorInnovation ] = []
  
  
  @IBOutlet private weak var tableView: UITableView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private var cellHeight: CGFloat = 80 {
    didSet { tableView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMDoorSystemsListViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    if let indexPath = tableView.indexPathForSelectedRow {
      tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMDoorSystemsListViewController {
  
  
  func loadData() {
    titleLabel.text = NSLocalizedString("Инновационные дверные системы", comment: "")
    items = Singletone.sharedInstance.doorInnovations
    
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMDoorSystemsListViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(FRMDoorSystemsListViewController.Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMDoorSystemsListViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMInnovationTableViewCell {
      let item = items[indexPath.row]
      cell.setData(item)
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    performSegue(Segue.segueListToDetail, sender: indexPath)
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMDoorSystemsListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueListToDetail {
      let destinitionController = segue.destinationViewController as! FRMDoorSystemDetailViewController
      let innovation = items[indexPath.row]
      
      destinitionController.innovation = innovation
    }
    
  }
  
  
}