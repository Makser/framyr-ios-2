//
//  FRMContactViewController.swift
//  Framyr
//
//  Created by Admin on 22.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit

class FRMContactViewController: FRMBaseViewController {

  
  
  //  MARK: -
  //  MARK: properties
  @IBOutlet private weak var nameIcon: UILabel!
  @IBOutlet private weak var phoneIcon: UILabel!
  @IBOutlet private weak var commentIcon: UILabel!
  @IBOutlet private weak var nameField: UITextField!
  @IBOutlet private weak var phoneField: UITextField!
  @IBOutlet private weak var commentView: UITextView!
  @IBOutlet private weak var sendButton: UIButton!
  @IBOutlet private weak var messageLabel: UILabel!
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMContactViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
    configureInterface()
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMContactViewController {
  
  
  func loadData() {
    titleLabel.text = NSLocalizedString("Обратная связь", comment: "")
    
    nameIcon.icon = Icomoon.Name
    phoneIcon.icon = Icomoon.Phone
    commentIcon.icon = Icomoon.Text
    
    nameField.placeholder = NSLocalizedString("Имя", comment: "")
    phoneField.placeholder = NSLocalizedString("Телефон", comment: "")
    
    
    
  }
  
  
}



//  MARK: -
//  MARK: interface
extension FRMContactViewController {
  
  
  func configureInterface() {
    nameIcon.textColor = AppColors.accent.color
    phoneIcon.textColor = AppColors.accent.color
    commentIcon.textColor = AppColors.accent.color
    
    nameField.textColor = AppColors.white.color
    phoneField.textColor = AppColors.white.color
    commentView.textColor = AppColors.white.color
    messageLabel.textColor = AppColors.white.color
    
    nameField.font = AppFonts.HelveticaNeueCyrLight.font(14)
    phoneField.font = AppFonts.HelveticaNeueCyrLight.font(14)
    commentView.font = AppFonts.HelveticaNeueCyrLight.font(14)
    messageLabel.font = AppFonts.HelveticaNeueCyrLight.font(17)
    
    nameField.tintColor = AppColors.white.color
    phoneField.tintColor = AppColors.white.color
    commentView.tintColor = AppColors.white.color
    
    commentView.textContainer.exclusionPaths = [UIBezierPath(rect: commentIcon.bounds)]
  }
  
  
}