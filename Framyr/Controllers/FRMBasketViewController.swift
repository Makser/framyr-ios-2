//
//  FRMBasketViewController.swift
//  Framyr
//
//  Created by Admin on 27.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMBasketViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  private var items: [ FRMGoods ] { return Singletone.sharedInstance.basket.getGoods() }
  
  
  @IBOutlet private weak var tableView: UITableView!;
  @IBOutlet private weak var priceTitleLabel: UILabel!
  @IBOutlet private weak var priceLabel: UILabel!
  @IBOutlet private weak var buyButton: UIButton!
  
  
  
  //  MARK: -
  //  MARK: constants
  private var cellHeight: CGFloat = 130 {
    didSet { tableView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMBasketViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureInterface()
    
    loadData()
  }
  
  
  override func configureRightBarButton() {
    addGearButton(Selector("gearTapped:"))
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMBasketViewController {
  
  
  func loadData() {
    titleLabel.text = NSLocalizedString("Корзина", comment: "")
    priceTitleLabel.text = NSLocalizedString("Общая стоимость покупки:", comment: "")
    buyButton.setTitle(NSLocalizedString("Оформить", comment: ""), forState: .Normal)
    
    updateTotalPrice()
  }
  
  
  func updateTotalPrice() {
    let totalPrice = Singletone.sharedInstance.basket.totalPrice
    
    buyButton.enabled = totalPrice != 0
    priceLabel.text = priceToString(totalPrice)
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDataSource
extension FRMBasketViewController: UITableViewDataSource {
  
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return items.count
  }
  
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(Reusable.cell, forIndexPath: indexPath)
    return cell!
  }
  
  
}



//  MARK: -
//  MARK: UITableViewDelegate
extension FRMBasketViewController: UITableViewDelegate {
  
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMBasketTableViewCell {
      let item = items[indexPath.row]
      
      cell.delegate = self
      cell.setData(item)
    }
    
  }
  
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//    performSegue(Segue.segueCollectionToDoors, sender: indexPath)
  }
  
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return cellHeight
  }
  
  
}



//  MARK: -
//  MARK: FRMBasketTableViewCellDelegate
extension FRMBasketViewController : FRMBasketTableViewCellDelegate {
  
  
  func minusTapped(cell: UITableViewCell) {
    guard let indexPath = tableView.indexPathForCell(cell) else { return }
    let item = items[indexPath.row]
    
    if item.count > 1 {
      item.count--
      Singletone.sharedInstance.basket.editGoods(item)
      tableView.reloadData() }
    else {
      MessageCenter.sharedInstance.showQuestion(
        NSLocalizedString("", comment: ""),
        message: NSLocalizedString("Удалить из корзины?", comment: ""),
        buttons: [
          (NSLocalizedString("Да", comment: ""), { ()->() in
            Singletone.sharedInstance.basket.removeGoods(item.goodsId)
            self.updateTotalPrice()
            self.tableView.reloadData()
            
          }),
          (NSLocalizedString("Нет", comment: ""), { ()->() in
            
            
          })
        ]) }
    
    updateTotalPrice()
  }
  
  
  func plusTapped(cell: UITableViewCell) {
    guard let indexPath = tableView.indexPathForCell(cell) else { return }
    let item = items[indexPath.row]
    
    item.count++
    Singletone.sharedInstance.basket.editGoods(item)
    tableView.reloadData()
    
    updateTotalPrice()
  }
  
  
}



//  MARK: -
//  MARK: actions
extension FRMBasketViewController {
  
  
  @IBAction func gearTapped(sender: AnyObject) {
    let cityVC = Storyboards.Main.instantiateBasketCityDialog()
    revealViewController().addChildViewController(cityVC)
    cityVC.didMoveToParentViewController(revealViewController())
    
    cityVC.view.frame = CGRectMake(0, 0, 300, 168)
    
    cityVC.completion = { () -> () in
      self.dismissPopupView()
      cityVC.removeFromParentViewController()
    }
    
    let config = STZPopupViewConfig()
    config.dismissTouchBackground = false
    config.cornerRadius = 2
    
    presentPopupView(cityVC.view, config: config)
    
  }
  
  
  @IBAction func buyTapped(sender: AnyObject) {
    
  }
  
  
}



//  MARK: -
//  MARK: interface
extension FRMBasketViewController {

  
  func configureInterface() {
    priceTitleLabel.font = AppFonts.HelveticaNeueCyrRoman.font(14)
    priceLabel.font = AppFonts.HelveticaNeueCyrRoman.font(17)
    
    priceTitleLabel.textColor = AppColors.color1.color
    priceLabel.textColor = AppColors.color1.color
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMBasketViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
//    if segue == Segue.segueCollectionToDoors {
//      let destinitionController = segue.destinationViewController as! FRMDoorListViewController
//      let collection = items[indexPath.row]
//      
//      destinitionController.collection = collection
//    }
    
  }
  
  
}