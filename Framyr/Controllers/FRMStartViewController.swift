//
//  ViewController.swift
//  Framyr
//
//  Created by Сергей Максимук on 09.11.15.
//  Copyright (c) 2015 sema. All rights reserved.
//

import UIKit

class FRMStartViewController: FRMBaseViewController {

  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    
    //  Проверяем наличие загруженных ранее данных по дате последнего обновления
    if let lastUpdateDate = Singletone.sharedInstance.lastUpdateDate {
      
      //  Проверяем дату последнего обновления сервера
      self.startUpdateChecking()
      FRMAPILayer.sharedInstance.checkLastUpdate { (date) -> () in
        guard let date = date else {
          //  Если не удалось получить дату, загружаем кэш
          self.preLoading()
          Singletone.sharedInstance.loadCachedData(self.postLoading, progress: self.updateLoadingProgress)
          return
        }
        
        //  Сравниваем лок. и глоб. даты обновлений
        if lastUpdateDate.compare(date) == .OrderedAscending {
          // Если версия сервера новее, получаем список обновлений
          FRMAPILayer.sharedInstance.getUpdateList(lastUpdateDate, completion: { (updates) -> () in
            if updates.count > 0 {
            // Если список обновлений не пуст, показываем его пользователю
            self.finishUpdateChecking()
              var message = ""
              for update in updates {
                switch update.0 {
                  case .Framyr: message += NSLocalizedString("Коллекция Фрамир", comment: "") + ": \(update.1)\n"
                  case .FinezaPuerta: message += NSLocalizedString("Коллекция Fineza Puerta", comment: "") + ": \(update.1)\n"
                  case .Colors: message += NSLocalizedString("Покрытия", comment: "") + ": \(update.1)\n"
                  case .Shops: message += NSLocalizedString("Магазины", comment: "") + ": \(update.1)\n"
                  case .StainedGlasses: message += NSLocalizedString("Витражи", comment: "") + ": \(update.1)\n"
                  case .Interior: message += NSLocalizedString("Интерьер", comment: "") + ": \(update.1)\n"
                  case .DoorSystems: message += NSLocalizedString("Дверные системы", comment: "") + ": \(update.1)\n"
                  case .News: message += NSLocalizedString("Новости", comment: "") + ": \(update.1)\n"
                }
              }
              
              MessageCenter.sharedInstance.showQuestion(
              NSLocalizedString("Нового в приложеии:", comment: ""),
              message: message,
              buttons: [
                (NSLocalizedString("Загрузить", comment: ""), { ()->() in
                  self.preLoading()
                  Singletone.sharedInstance.downloadData(self.postLoading, progress: self.updateLoadingProgress)
                  
                }),
                (NSLocalizedString("Отменить", comment: ""), { ()->() in
                  self.preLoading()
                  Singletone.sharedInstance.loadCachedData(self.postLoading, progress: self.updateLoadingProgress)
                  
                })
              ])
            }
            
            else {
              //  Если список обновлений пуст, загружаем кэш
              self.preLoading()
              Singletone.sharedInstance.loadCachedData(self.postLoading, progress: self.updateLoadingProgress)
              
            }
            
          })
          
          
          
        } else {
          // Если версии совпадают, загружаем кэш
          self.preLoading()
          Singletone.sharedInstance.loadCachedData(self.postLoading, progress: self.updateLoadingProgress)
          
        }
      }
      
    } else {
      // Если данные ранее не были загружены, загружаем с сервера
      NSLog("First run")
      self.preLoading()
      Singletone.sharedInstance.downloadData(self.postLoading, progress: self.updateLoadingProgress)

    }
    
  }
  
  
  
  //  MARK: -
  //  MARK: interaction
  func startUpdateChecking() {
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      MessageCenter.sharedInstance.showLoading(NSLocalizedString("Проверка обновлений", comment: ""))
    }
    
  }
  
  
  func finishUpdateChecking() {
    MessageCenter.sharedInstance.hide()
    
  }
  
  
  func preLoading() {
    finishUpdateChecking()
    dispatch_async(dispatch_get_main_queue()) { () -> Void in
      MessageCenter.sharedInstance.showProgress(NSLocalizedString("Загрузка", comment: ""), progress: 0.0)
    }
    
  }
  
  
  func updateLoadingProgress(progress: Double) {
    MessageCenter.sharedInstance.showProgress(NSLocalizedString("Загрузка", comment: ""), progress: progress)

  }
  
  
  func postLoading(updated: Bool) {
    if updated {
      NSLog("Data was updated")
      MessageCenter.sharedInstance.showToast(NSLocalizedString("Загружены последние обновления", comment: ""))
    
    } else {
      NSLog("No data to update")
      MessageCenter.sharedInstance.showToast(NSLocalizedString("Нет обновлений", comment: ""))
      
    }
    
    MessageCenter.sharedInstance.hide()
    self.revealViewController().revealToggle(self)
//    self.kSideMenuController?.showLeftViewAnimated(true, completionHandler: nil)
    
  }
  
  
}

