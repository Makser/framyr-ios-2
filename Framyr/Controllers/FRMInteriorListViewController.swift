//
//  FRMInteriorListViewController.swift
//  Framyr
//
//  Created by Admin on 21.12.15.
//  Copyright © 2015 sema. All rights reserved.
//

import UIKit


class FRMInteriorListViewController: FRMBaseViewController {
  
  
  
  //  MARK: -
  //  MARK: properties
  var interior: FRMinterior?
  var stainedGlass: FRMStainedGlassCategory?
  
  
  private var headerText: String = ""
  private var items: [ AnyObject ] = []
  
  
  @IBOutlet private weak var collectionView: UICollectionView!;
  
  
  
  //  MARK: -
  //  MARK: constants
  private let cellInset: CGFloat = 4
  private var cellSize: CGSize = CGSizeZero {
    didSet { collectionView.reloadData() }
  }
  private var headerSize: CGSize = CGSizeZero {
    didSet { collectionView.reloadData() }
  }
  
  
}



//  MARK: -
//  MARK: VLC
extension FRMInteriorListViewController {
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    loadData()
  }
  
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let cellWidth = collectionView.bounds.size.width / 2 - 2 * cellInset
    let cellHeight = 3 / 2 * cellWidth
    cellSize = CGSizeMake(cellWidth, cellHeight)
    
    headerSize = calculateHeaderSize()
//    let headerWidth = collectionView.bounds.size.width - 2 * cellInset
//    headerPrototype.setData(headerText)
//    headerSize = headerPrototype.sizeThatFits(CGSizeMake(headerWidth, 0))
  }
  
  
}



//  MARK: -
//  MARK: data
extension FRMInteriorListViewController {
  
  
  func loadData() {
    if let interior = interior {
      titleLabel.text = interior.name
      items = [interior]
      headerText = interior.descr }
    else if let stainedGlass = stainedGlass {
      titleLabel.text = stainedGlass.name
      items = stainedGlass.glasses
      headerText = stainedGlass.descr }
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDataSource
extension FRMInteriorListViewController: UICollectionViewDataSource {
  
  
  func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
    return 2
  }
  
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    switch section {
    case 0: return 1
    case 1: return items.count
    default: return 0
    }

  }
  
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    switch indexPath.section {
    case 0: return collectionView.dequeueReusableCell(FRMInteriorListViewController.Reusable.header, forIndexPath: indexPath)!
    case 1: return collectionView.dequeueReusableCell(FRMInteriorListViewController.Reusable.cell, forIndexPath: indexPath)!
    default: return UICollectionViewCell()
    }
    
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDelegate
extension FRMInteriorListViewController: UICollectionViewDelegate {
  
  
  func calculateHeaderSize() -> CGSize {
    if headerSize == CGSizeZero {
      let cell = self.collectionView.dequeueReusableCell(Reusable.header, forIndexPath: NSIndexPath(forItem: 0, inSection: 0)) as! FRMInteriorHeaderCollectionViewCell
      cell.setData(headerText)
      
      let headerWidth = collectionView.bounds.size.width - 3 * cellInset
      cell.frame.size.width = headerWidth
      let headerHeight = cell.preferredHeight()
      
      return CGSizeMake(headerWidth, headerHeight)
    }
    
    return headerSize
  }
  
  
  func collectionView(collectionView: UICollectionView, willDisplayCell cell: UICollectionViewCell, forItemAtIndexPath indexPath: NSIndexPath) {
    if let cell = cell as? FRMInteriorCollectionViewCell {
      let item = items[indexPath.row]
      switch item {
      case let interior as FRMinterior: cell.setData(interior)
      case let glass as FRMStainedGlass: cell.setGlassData(glass)
      default: break
      }
 
    }
    if let cell = cell as? FRMInteriorHeaderCollectionViewCell {
      cell.setData(headerText)
      
    }
    
  }
  
  
  func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
    performSegue(Segue.segueListToDetail, sender: indexPath)
  }
  
  
}



//  MARK: -
//  MARK: UICollectionViewDelegateFlowLayout
extension FRMInteriorListViewController: UICollectionViewDelegateFlowLayout {
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    switch indexPath.section {
    case 0: return headerSize
    case 1: return cellSize
    default: return CGSizeZero
    }
    
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    return cellInset
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return cellInset * 2
  }
  
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsMake(cellInset, cellInset, cellInset, cellInset)
  }
  
  
}



//  MARK: -
//  MARK: Navigation
extension FRMInteriorListViewController {
  
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    guard let indexPath = sender as? NSIndexPath else { return }
    
    if segue == Segue.segueListToDetail {
      let destinitionController = segue.destinationViewController as! FRMInteriorDetailViewController
      let item = items[indexPath.row]
      
      switch item {
      case let interior as FRMinterior: destinitionController.interior = interior
      case let stainedGlass as FRMStainedGlass: destinitionController.stainedGlass = stainedGlass
      default: break
      }
      
      
    }
    
  }
  
  
}